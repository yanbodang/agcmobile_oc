//
//  AGCUtility.h
//  AGCFramework
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIViewController;
@class AGCEmailModel;

@interface AGCUtility : NSObject

+(NSString *)date:(NSDate *)date format:(NSString *)format;
+(NSDate*)dataFromString:(NSString*)sDate format:(NSString*)format;
+(NSDate*)dateFromString:(NSString*)sDate formats:(NSArray<__kindof NSString*> *)formats;
+(NSArray<__kindof NSString*>*)getDataFormates;


+(NSString *) getUUID;
+(NSString *)getUniqueGUID;

+(void)showAlert:(NSString*)title message:(NSString*)msg onController:(__weak UIViewController*)controller
   defaultAction:(void (^)(void))defaultAction defaultActionTitle:(NSString*)defaultActionTitle;

+(void)showAlert:(NSString*)title message:(NSString*)msg onController:(__weak UIViewController*)controller
  positiveAction:(void (^)(void))positiveAction positiveActionTitle:(NSString*)positiveActionTitle
   negtiveAction:(void (^)(void))negtiveAction negtiveActionTitle:(NSString*)negtiveActionTitle;

+(BOOL)verifyEmail:(NSString*)email;


+(void)saveUserDefaultValue:(id)value forKey:(NSString*)key;
+(id)getUserDefaultValueForKey:(NSString*)key;


+(NSError*)sendEmail:(AGCEmailModel*)email onViewController:(UIViewController*)viewController;

@end
