//
//  AGCPhotoCollectionViewModel.h
//  AGCFramework
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "BaseViewModel.h"

@class AGCPhotoModel;
@class UIImage;

@interface AGCPhotoCollectionViewModel : BaseViewModel

@property(nonatomic, strong)NSArray * photos;

-(BOOL)appendPhoto:(UIImage*)image comment:(NSString*)comment;
-(BOOL)removePhoto:(AGCPhotoModel*)photoModel;

@end
