//
//  MainMenuTableViewCell.m
//  AGCFramework
//
//  Created by Yanbo Dang on 22/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "MainMenuTableViewCell.h"
#import "MainMenuViewModel.h"
#import "AGCStyle.h"
#import "NSObject+PropertyName.h"

#define WIDTH_MENU_ICON 30.0
#define HEIGHT_MENU_ICON 30.0

@interface MainMenuTableViewCell()

@property(nonatomic, strong)UIView *viewSelIndicator;
@property(nonatomic, strong)UIImageView * menuIcon;
@property(nonatomic, strong)UILabel * lblMenuTitle;

-(void)createMenuCell;
-(void)changeBindingContext:(nullable id)bindingContext;

@end

@implementation MainMenuTableViewCell

@synthesize bindingContext = _bindingContext;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self createMenuCell];
    return self;
}

#pragma mark - Private Context

-(void)setBindingContext:(id)bindingContext
{
    if(nil != self.bindingContext)
       [self.bindingContext removeObserver:self];
    
    _bindingContext = bindingContext;
    [self changeBindingContext:bindingContext];
    
    [self.bindingContext addObserver:self forKeyPath:@"bSelected" options:NSKeyValueObservingOptionNew context:nil];
}

-(void)changeBindingContext:(nullable id)bindingContext
{
    MainMenuViewModel * vm = (MainMenuViewModel*)bindingContext;
    
    self.lblMenuTitle.text = vm.menuTitle;
    self.menuIcon.image = [UIImage imageNamed:vm.menuIconName];
    
    if(vm.bSelected)
        self.viewSelIndicator.backgroundColor = [AGCStyle darkGreen];
    else
        self.viewSelIndicator.backgroundColor = [UIColor clearColor];
}

-(void)createMenuCell
{
    self.viewSelIndicator = [[UIView alloc] init];
    self.viewSelIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.menuIcon = [[UIImageView alloc] init];
    self.menuIcon.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.lblMenuTitle = [[UILabel alloc] init];
    self.lblMenuTitle.translatesAutoresizingMaskIntoConstraints = NO;
    self.lblMenuTitle.font = [UIFont systemFontOfSize:18];
    
    [self.contentView addSubview:self.viewSelIndicator];
    [self.contentView addSubview:self.menuIcon];
    [self.contentView addSubview:self.lblMenuTitle];
    
    /*
    NSDictionary * views = @{@"viewSelIndicator" : self.viewSelIndicator,
                             @"menuIcon" : self.menuIcon,
                             @"lblMenuTitle" : self.lblMenuTitle};
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[viewSelIndicator(4)]-8-[menuIcon]-8-[lblMenuTitle]" options:0 metrics:nil views:views]];
     
     */
    
    //
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.viewSelIndicator attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:0.0 constant:4.0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.viewSelIndicator attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.viewSelIndicator attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.viewSelIndicator attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    //
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.menuIcon attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.viewSelIndicator attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:8.0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.menuIcon attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.menuIcon attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:0.0 constant:HEIGHT_MENU_ICON]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.menuIcon attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:0.0 constant:WIDTH_MENU_ICON]];
    
    //
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.lblMenuTitle attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.lblMenuTitle attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.menuIcon attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:8.0]];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    [self changeBindingContext:self.bindingContext];
}
@end
