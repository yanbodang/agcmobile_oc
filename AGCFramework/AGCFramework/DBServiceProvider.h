//
//  DBServiceProvider.h
//  AGCFramework
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBServiceProvider : NSObject


+(NSString *)creatDBWithName:(NSString*)dbName;

+(BOOL)executeSql:(NSString*)sSQL onDB:(NSString * )dbName;

+(BOOL)executeUpdate:(NSString*)sSql onDB:(NSString *)dbName;

+(BOOL)executeUpdate:(NSString*)sSql withParam:(NSDictionary*)param onDB:(NSString *)dbName;

+(BOOL)executeUpdate:(NSString*)sSql withKeyValuePairs:(NSArray*)pairs onDB:(NSString *)dbName;

+(BOOL)querySql:(NSString*)sSQL onDB:(NSString * )dbName queryResult:(void (^)(id result, BOOL finished))result;

/**
 
 Insert or replace an item on a table by NSDictionary
 
 @param tableName table name.
 @param pair key-value pairs (NSDictionary).
 @param dbName db full path
 @return BOOL
 
 */
+(BOOL)insertOrReplaceOnTable:(NSString*)tableName withKeyValuePair:(NSDictionary*)pair onDB:(NSString*)dbName;


/**
 
 Insert or replace items on a table by Array of NSDictionary
 It can insert or update many records on an table
 
 @param tableName table name.
 @param pairs array of key-value pairs.
 @param dbName db full path
 @return BOOL
 
 */
+(BOOL)insertOrReplaceOnTable:(NSString*)tableName withKeyValuePairs:(NSArray*)pairs onDB:(NSString*)dbName;

@end
