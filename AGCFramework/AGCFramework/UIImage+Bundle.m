//
//  UIImage+Bundle.m
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "UIImage+Bundle.h"

@implementation UIImage (Bundle)

+(instancetype)imageNamed:(NSString*)sImageName inBundleNamed:(NSString*)bundleName
{
    NSString * path = [[NSBundle mainBundle] pathForResource:bundleName ofType:@"bundle"];
    NSBundle * bundle = [NSBundle bundleWithPath:path];
    
    return [UIImage imageNamed:sImageName inBundle:bundle compatibleWithTraitCollection:nil];
}

+(instancetype)agcResourceImageNamed:(NSString*)sImageName
{
    return [UIImage imageNamed:sImageName inBundleNamed:@"AGCResource"];
}

@end
