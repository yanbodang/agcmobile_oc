//
//  BaseViewModel.h
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISaving.h"


@interface BaseViewModel : NSObject <ISaving>

-(void)initViewModelWithParam:(id)param completed:(void (^)(id))completedHandle;

@end
