//
//  AGCPhotoCollectionCell.h
//  AGCFramework
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCBaseCollectionViewCell.h"

@protocol AGCPhotoCollectionCellDelegate <NSObject>

@optional

-(void)requestRemove:(id)model;

@end

@interface AGCPhotoCollectionCell : AGCBaseCollectionViewCell

@property(nonatomic,weak)id<AGCPhotoCollectionCellDelegate> delegate;

@end
