//
//  AGCBaseView.h
//  AGCFramework
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISaving.h"

@interface AGCBaseView : UIView<ISaving>

@property (nonatomic, strong)id bindingContext;

@end
