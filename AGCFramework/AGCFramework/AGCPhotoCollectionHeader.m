//
//  AGCPhotoCollectionHeader.m
//  AGCFramework
//
//  Created by Yanbo Dang on 31/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCPhotoCollectionHeader.h"
#import "AGCStyle.h"

@interface AGCPhotoCollectionHeader()

@property(nonatomic, strong)UILabel * lblTitle;
@property(nonatomic, strong)UIButton * btnAdd;
@property(nonatomic, strong)UIView * vDecoration;


-(void)createHeader;
-(void)addPhoto:(id)sender;

@end

@implementation AGCPhotoCollectionHeader

-(instancetype)init
{
    self = [super init];
    [self createHeader];
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self createHeader];
    return self;
}

-(void)createHeader
{
    self.backgroundColor = [AGCStyle agcLightBlack];
    
    self.lblTitle = [[UILabel alloc] init];
    self.lblTitle.text = @"Manage Photo(s)";
    self.lblTitle.textColor = [AGCStyle agcWhite];
    self.lblTitle.font = [UIFont boldSystemFontOfSize:16];
    self.lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnAdd setBackgroundImage:[UIImage imageNamed:@"iconAddWhite"] forState:UIControlStateNormal];
    self.btnAdd.translatesAutoresizingMaskIntoConstraints = NO;
    [self.btnAdd addTarget:self action:@selector(addPhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    self.vDecoration = [[UIView alloc] init];
    self.vDecoration.backgroundColor = [AGCStyle darkOringe];
    self.vDecoration.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addSubview:self.lblTitle];
    [self addSubview:self.btnAdd];
    [self addSubview:self.vDecoration];
    
    NSDictionary * views = @{@"lblTitle" : self.lblTitle,
                             @"btnAdd" : self.btnAdd,
                             @"vDecoration" : self.vDecoration};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[vDecoration(4)]-8-[lblTitle]" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnAdd(24)]-8-|" options:0 metrics:nil views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[vDecoration]|" options:0 metrics:nil views:views]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btnAdd attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    [self.btnAdd addConstraint:[NSLayoutConstraint constraintWithItem:self.btnAdd attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:0.0 constant:24.0]];
}

-(void)addPhoto:(id)sender
{
    if([self.delegate respondsToSelector:@selector(requestAddPhoto:)])
    {
        [self.delegate requestAddPhoto:self];
    }
}


@end
