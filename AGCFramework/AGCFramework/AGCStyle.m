//
//  AGCStyle.m
//  AGCFramework
//
//  Created by Yanbo Dang on 17/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCStyle.h"
#import <UIKit/UIKit.h>
#import "UIColor+HexString.h"

#define ORINGE_DARK @"#EF6809"
#define GREEN_DARK @"#9F9C2B"
#define BLACK_DARK @"#231F20"
#define BLACK_LIGHT @"#333333"

#define HEIGHT_MAIN_MENU_SECION_HEADER 45.0

@interface AGCStyle()

@end


@implementation AGCStyle


+(UIColor *)darkOringe
{
    return [UIColor colorFromHexString:ORINGE_DARK];
}

+(UIColor *)darkGreen
{
    return [UIColor colorFromHexString:GREEN_DARK];
}

+(UIColor *)agcDarkBlack
{
    return [UIColor colorFromHexString:BLACK_DARK];
}

+(UIColor *)agcLightBlack
{
    return [UIColor colorFromHexString:BLACK_LIGHT];
}

+(UIColor *)agcWhite
{
    return [UIColor whiteColor];
}

+(CGFloat)heightMainMenuSectionHeader
{
    return HEIGHT_MAIN_MENU_SECION_HEADER;
}

@end
