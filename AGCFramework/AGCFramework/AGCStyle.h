//
//  AGCStyle.h
//  AGCFramework
//
//  Created by Yanbo Dang on 17/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@class UIColor;

@interface AGCStyle : NSObject

+(UIColor *)darkOringe;
+(UIColor *)darkGreen;
+(UIColor *)agcDarkBlack;
+(UIColor *)agcLightBlack;
+(UIColor *)agcWhite;

+(CGFloat)heightMainMenuSectionHeader;


@end
