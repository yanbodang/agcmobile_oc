//
//  AGCUtility.m
//  AGCFramework
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGCUtility.h"
#import "Constants.h"
#import "AGCEmailModel.h"

#import "MFMailComposeViewController+AGC.h"
#import <MessageUI/MFMailComposeViewController.h>


@interface AGCUtility()

+(NSError *)errorWithTitle:(NSString*)title message:(NSString*)message code:(NSInteger)code;

@end

@implementation AGCUtility

+(NSError *)errorWithTitle:(NSString*)title message:(NSString*)message code:(NSInteger)code
{
    return [NSError errorWithDomain:@"AGC" code:code userInfo:@{@"Title" : title,
                                                             @"Message" : message}];
}

+(NSString *)date:(NSDate *)date format:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat :format];
    return [dateFormatter stringFromDate:date];
}

+(NSDate*)dataFromString:(NSString*)sDate format:(NSString*)format
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormat setDateFormat:format];
    
    return [dateFormat dateFromString:sDate];
}

+(NSDate*)dateFromString:(NSString*)sDate formats:(NSArray<__kindof NSString*> *)formats
{
    NSDate * date = nil;
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    for(NSString * sFormat in formats)
    {
        dateFormatter.dateFormat = sFormat;
        date = [dateFormatter dateFromString:sDate];
        if(nil != date)
            break;
    }
    
    return date;
}

+(NSArray<__kindof NSString*>*)getDataFormates
{
    return @[@"yyyy-MM-dd",@"HH:mm a",@"HH.mm a"];
}

+(NSString *) getUUID {
    NSUUID  *UUID = [NSUUID UUID];
    NSString* stringUUID = [UUID UUIDString];
    return stringUUID;
}

+(NSString *)getUniqueGUID {
    NSString *UUID = [[NSUUID UUID] UUIDString];
    return UUID;
}

+(void)showAlert:(NSString*)title message:(NSString*)msg onController:(UIViewController*)controller
   defaultAction:(void (^)(void))defaultAction defaultActionTitle:(NSString*)defaultActionTitle
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(nil == controller)
            return;
        
        UIAlertAction* action = [UIAlertAction actionWithTitle:defaultActionTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            BLOCK_SAFE_RUN(defaultAction)
        }];
        
        UIAlertController * alertCtrl = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alertCtrl addAction:action];
        
        [controller presentViewController:alertCtrl animated:YES completion:nil];
    });
}

+(void)showAlert:(NSString*)title message:(NSString*)msg onController:(UIViewController*)controller
  positiveAction:(void (^)(void))positiveAction positiveActionTitle:(NSString*)positiveActionTitle
   negtiveAction:(void (^)(void))negtiveAction negtiveActionTitle:(NSString*)negtiveActionTitle
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if(nil == controller)
            return;
        
        UIAlertAction* pAction = [UIAlertAction actionWithTitle:positiveActionTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            BLOCK_SAFE_RUN(positiveAction)
        }];
        
        UIAlertAction* nAction = [UIAlertAction actionWithTitle:negtiveActionTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            BLOCK_SAFE_RUN(negtiveAction)
        }];
        
        UIAlertController * alertCtrl = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alertCtrl addAction:pAction];
        [alertCtrl addAction:nAction];
        
        [controller presentViewController:alertCtrl animated:YES completion:nil];
    });
}

+(BOOL)verifyEmail:(NSString*)email
{
    NSError * err = nil;
    NSString * pattern =  @"^([\\w\\d\\-\\.]+)@{1}(([\\w\\d\\-]{1,67})|([\\w\\d\\-]+\\.[\\w\\d\\-]{1,67}))\\.(([a-zA-Z\\d]{2,4})(\\.[a-zA-Z\\d]{2})?)$";
    
    NSRegularExpression * regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&err];
    
    NSUInteger numOfMatches = [regex numberOfMatchesInString:email options:0 range:NSMakeRange(0, email.length)];
    
    return numOfMatches > 0;
}

+(void)saveUserDefaultValue:(id)value forKey:(NSString*)key
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:value forKey:key];
    [userDefaults synchronize];
}

+(id)getUserDefaultValueForKey:(NSString*)key
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults valueForKey:key];
}


+(NSError*)sendEmail:(AGCEmailModel*)email onViewController:(UIViewController*)viewController
{
    MFMailComposeViewController * composer=[[MFMailComposeViewController alloc]init];
    NSError * error = nil;
    
    composer.mailComposeDelegate = (id<MFMailComposeViewControllerDelegate>)viewController;

    if ([MFMailComposeViewController canSendMail])
    {
        [composer setToRecipients:email.recipients]; // change to generic email account
        [composer setSubject:email.subject];
        [composer setMessageBody:email.message isHTML:YES];
        [composer setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        NSData * attData = [NSData dataWithContentsOfFile:email.attachmentPath];
        
        [composer addAttachmentData:attData mimeType:email.attachmentMimeType fileName:email.attachmentPath];
        
        [viewController presentViewController:composer animated:YES completion: ^{
            return;
        }];
    }
    else
        error = [AGCUtility errorWithTitle:SERROR message:@"Unable to send email" code:-1];
    
    return error;
}

@end
