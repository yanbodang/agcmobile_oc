//
//  AGCPhotoCollectionViewController.m
//  AGCFramework
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCPhotoCollectionViewController.h"
#import "AGCPhotoCollectionCell.h"
#import "AGCPhotoCollectionViewModel.h"
#import "AGCPhotoCollectionHeader.h"

static NSString * COLLECTION_CELL_ID = @"AGCPhotoCollectionCellID";
static NSString * COLLECTION_HEADER_ID = @"AGCPhotoCollectionHeaderID";

@interface AGCPhotoCollectionViewController ()<UICollectionViewDelegateFlowLayout,AGCPhotoCollectionHeaderDelegate,AGCPhotoCollectionCellDelegate>

@end

@implementation AGCPhotoCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.collectionView.bounces = NO;
    self.collectionView.showsVerticalScrollIndicator = NO;
    
    [self.collectionView registerClass:[AGCPhotoCollectionCell class] forCellWithReuseIdentifier:COLLECTION_CELL_ID];
    [self.collectionView registerClass:[AGCPhotoCollectionHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:COLLECTION_HEADER_ID];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(UICollectionViewLayout*)getCollectionViewLayout
{
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionHeadersPinToVisibleBounds = YES;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    return layout;
}

#pragma mark - UICollectionViewDataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return ((AGCPhotoCollectionViewModel*)self.bindingContext).photos.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AGCPhotoCollectionCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:COLLECTION_CELL_ID forIndexPath:indexPath];
    cell.delegate = self;
    cell.bindingContext = [((AGCPhotoCollectionViewModel*)self.bindingContext).photos objectAtIndex:indexPath.row];
    
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView * view = nil;
    if([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        AGCPhotoCollectionHeader * header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:COLLECTION_HEADER_ID forIndexPath:indexPath];
        header.delegate = self;
        
        view = header;
    }
    return view;
}

#pragma mark - UICollectionViewDelegateFlowLayout

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = collectionView.frame.size.width / 2 - 5 * 2;
    CGFloat height = width + 30;// * 1.5 - 5 * 2;
    
    return CGSizeMake(width,height);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(collectionView.frame.size.width, 45.0);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

@end
