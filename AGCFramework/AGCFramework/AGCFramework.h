//
//  AGCFramework.h
//  AGCFramework
//
//  Created by Yanbo Dang on 15/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AGCFramework.
FOUNDATION_EXPORT double AGCFrameworkVersionNumber;

//! Project version string for AGCFramework.
FOUNDATION_EXPORT const unsigned char AGCFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AGCFramework/PublicHeader.h>

#import "AGCBaseViewController.h"
#import "AGCBaseTableViewController.h"
#import "AGCBaseCollectionViewController.h"
#import "AGCBaseTabBarController.h"
#import "AGCBaseNavigationController.h"
#import "AGCBaseTableView.h"
#import "AGCMainMenuViewController.h"
#import "AGCBaseTableViewCell.h"
#import "AGCBaseButton.h"
#import "UIFloatLabelTextField.h"
#import "AGCBaseView.h"
#import "SignatureView.h"
#import "AGCTextView.h"
#import "KLCPopup.h"


#import "AGCMain.h"
#import "ISaving.h"
#import "IBinding.h"
#import "ITheme.h"
#import "AGCLog.h"
#import "Constants.h"
#import "AGCStyle.h"
#import "AGCUtility.h"
#import "IAGCTableViewDelegate.h"

#import "AGCPhotoCollectionViewController.h"
#import "AGCPhotoCollectionViewModel.h"
#import "AGCPhotoCollectionHeader.h"
#import "AGCBaseCollectionViewCell.h"
#import "AGCPhotoCollectionCell.h"
#import "AGCPhotoModel.h"


#import "BaseViewModel.h"
#import "MainMenuViewModel.h"
#import "BaseModel.h"
#import "AGCEmailModel.h"

#import "AGCHttpClient.h"
#import "AGCHttpClientConfiguration.h"
#import "DBServiceProvider.h"


#import "UIImage+Bundle.h"
#import "NSObject+PropertyName.h"
#import "UIView+AGC.h"

