//
//  AGCBaseTableViewController.m
//  AGCFramework
//
//  Created by Yanbo Dang on 15/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCBaseTableViewController.h"
#import "UIViewController+AGCFoundation.h"
#import "AGCBaseTableView.h"
#import "AGCStyle.h"
#import "UIImage+Bundle.h"

@interface AGCBaseTableViewController ()

-(void)initTableView;

@end

@implementation AGCBaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self initTableView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITabelViewDataSource

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * sectionHeaderView = [[UIView alloc] init];
    sectionHeaderView.backgroundColor = [AGCStyle agcLightBlack];
    
    
    UILabel * lblSectionTitle = [[UILabel alloc] init];
    lblSectionTitle.text = [self headerTitleForSecion:section];
    lblSectionTitle.textColor = [AGCStyle agcWhite];
    lblSectionTitle.font = [UIFont boldSystemFontOfSize:16.0];
    lblSectionTitle.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIView * coloredView = [[UIView alloc] init];
    coloredView.translatesAutoresizingMaskIntoConstraints = NO;
    coloredView.backgroundColor = [AGCStyle darkOringe];
    
    
    [sectionHeaderView addSubview:lblSectionTitle];
    [sectionHeaderView addSubview:coloredView];
    
    NSDictionary * views = NSDictionaryOfVariableBindings(lblSectionTitle,coloredView,sectionHeaderView);
    
    [sectionHeaderView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[coloredView(4)]-8-[lblSectionTitle]" options:0 metrics:nil views:views]];
    
    [sectionHeaderView addConstraint:[NSLayoutConstraint constraintWithItem:lblSectionTitle attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:sectionHeaderView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    
    [sectionHeaderView addConstraint:[NSLayoutConstraint constraintWithItem:coloredView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:sectionHeaderView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0]];
    [sectionHeaderView addConstraint:[NSLayoutConstraint constraintWithItem:coloredView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:sectionHeaderView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    return sectionHeaderView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [AGCStyle heightMainMenuSectionHeader];
}


#pragma mark - Private Methods
-(void)initTableView
{
    self.tableView = [[AGCBaseTableView alloc] init];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    NSDictionary * views = @{@"view" : self.view,
                             @"tableView" : self.tableView,
                             @"bottomGuide" : self.bottomLayoutGuide,
                             @"topGuide" : self.topLayoutGuide};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topGuide][tableView][bottomGuide]" options:0 metrics:nil views:views]];
}

@end
