//
//  Constants.h
//  AGCFramework
//
//  Created by Yanbo Dang on 15/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#ifndef AGCFramework_Constants_h
#define AGCFramework_Constants_h

#define BLOCK_SAFE_RUN(block, ...) block ? block(__VA_ARGS__) : nil;

#pragma mark - API

extern NSString* API_WSBASE;
extern NSString* API_GETALLJOBS;
extern NSString* API_GETALLPREFERENCES;
extern NSString* API_POSTDATA_V2;


#pragma mark - String Constants

extern NSString* SEmpty;
extern NSString* SOK;
extern NSString* SYES;
extern NSString* SNO;
extern NSString* SCANCEL;
extern NSString* NA;
extern NSString* SAVE;
extern NSString* SAVED;
extern NSString* SUCCESS;
extern NSString* SERROR;
extern NSString* CLOSE;
extern NSString* SUBMIT;

#endif /* AGCFramework_Constants_h */
