//
//  UIColor+HexString.m
//  AGCFramework
//
//  Created by Yanbo Dang on 17/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "UIColor+HexString.h"

@implementation UIColor (HexString)

+(UIColor*)colorFromHexString:(NSString*)sColor
{
    NSString * sHexColor = [sColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSScanner * scanner = [NSScanner scannerWithString:sHexColor];
    
    if([sHexColor hasPrefix:@"#"]) scanner.scanLocation = 1;
    
    UInt32 nColor = 0;
    [scanner scanHexInt:&nColor];
    
    UInt32 nMask = 0x000000FF;
    
    int r = (nColor >> 16) & nMask;
    int g = (nColor >> 8) & nMask;
    int b = nColor & nMask;
    
    return [self colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:0xFF];
}

@end
