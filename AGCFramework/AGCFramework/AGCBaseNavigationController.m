//
//  AGCBaseNavigationController.m
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCBaseNavigationController.h"
#import "AGCStyle.h"
#import "UIImage+Bundle.h"

@interface AGCBaseNavigationController ()
@end

@implementation AGCBaseNavigationController

-(instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Private Methods

-(UIViewController*)getRootViewController
{
    return [self.viewControllers firstObject];
}


@end
