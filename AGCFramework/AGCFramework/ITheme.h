//
//  ITheme.h
//  AGCFramework
//
//  Created by Yanbo Dang on 22/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ITheme <NSObject>

@optional

-(void)applyTheme:(id)param;

//-(NSArray <__kindof UIBarButtonItem *> *)getRightBarItems;

//-(NSArray <__kindof UIBarButtonItem *> *)getLeftBarItems;

@end
