//
//  AGCBaseViewController.m
//  AGCFramework
//
//  Created by Yanbo Dang on 15/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCBaseViewController.h"
#import "UIViewController+AGCFoundation.h"

@interface AGCBaseViewController ()

-(void)addRightBarItems:(NSArray<__kindof UIBarButtonItem *> *)items;

-(void)addLeftBarItems:(NSArray<__kindof UIBarButtonItem *> *)items;

@end

@implementation AGCBaseViewController

#pragma mark - Life Cycle Method

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self enableSideMenuGesture];
    [self applyTheme:nil];

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    [self addLeftBarItems:self.leftBarItems];
    [self addRightBarItems:self.rightBarItems];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self saveModel:self.bindingContext];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reportError:(NSError * )error
{
    
}


-(void)addRightBarItems:(NSArray<__kindof UIBarButtonItem *> *)items
{
    self.navigationItem.rightBarButtonItems = items;
}

-(void)addLeftBarItems:(NSArray<__kindof UIBarButtonItem *> *)items
{
    if(nil != items)
        self.navigationItem.leftBarButtonItems = items;
}

-(BOOL)saveModel:(id)model
{
    BOOL bRslt = YES;
    
    if([self.bindingContext respondsToSelector:@selector(save)])
    {
        bRslt = [self.bindingContext save];
    }
    
    return bRslt;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
