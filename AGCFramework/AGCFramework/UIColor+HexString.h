//
//  UIColor+HexString.h
//  AGCFramework
//
//  Created by Yanbo Dang on 17/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexString)

+(UIColor*)colorFromHexString:(NSString*)sColor;

@end
