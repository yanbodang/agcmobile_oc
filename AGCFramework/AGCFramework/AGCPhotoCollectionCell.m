//
//  AGCPhotoCollectionCell.m
//  AGCFramework
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCPhotoCollectionCell.h"
#import "AGCStyle.h"
#import "AGCPhotoModel.h"
#import "UIImage+Bundle.h"

@interface AGCPhotoCollectionCell()

@property(nonatomic, strong)UIImageView * imgView;
@property(nonatomic, strong)UILabel * lblComment;
@property(nonatomic, strong)UIView * commentBk;
@property(nonatomic, strong)UIButton * btnRemove;

-(void)createCell;

-(void)onBindingContextChanged:(AGCPhotoModel*)model;

-(void)remove:(id)sender;

@end


@implementation AGCPhotoCollectionCell

@synthesize bindingContext = _bindingContext;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)init
{
    self = [super init];
    [self createCell];
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self createCell];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self createCell];
}

-(void)createCell
{
    self.layer.cornerRadius = 5;
    self.backgroundColor = [AGCStyle agcLightBlack];
    
    self.imgView = [[UIImageView alloc] init];
    self.imgView.translatesAutoresizingMaskIntoConstraints = NO;
    self.imgView.contentMode = UIViewContentModeScaleToFill;
    
    self.lblComment = [[UILabel alloc] init];
    self.lblComment.translatesAutoresizingMaskIntoConstraints = NO;
    self.lblComment.textColor = [AGCStyle agcWhite];
    self.lblComment.font = [UIFont systemFontOfSize:12];
    self.lblComment.textAlignment = NSTextAlignmentLeft;
    self.lblComment.numberOfLines = 0;
    self.lblComment.lineBreakMode = NSLineBreakByTruncatingTail;
    
    self.commentBk = [[UIView alloc] init];
    self.commentBk.translatesAutoresizingMaskIntoConstraints = NO;
    self.commentBk.backgroundColor = [[AGCStyle darkOringe] colorWithAlphaComponent:0.8];
    [self.commentBk addSubview:self.lblComment];
    
    self.btnRemove = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnRemove.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIImage * img = [UIImage agcResourceImageNamed:@"iconRemoveWhite"];
    [self.btnRemove setImage:img forState:UIControlStateNormal];
    [self.btnRemove addTarget:self action:@selector(remove:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.imgView];
    [self addSubview:self.commentBk];
    [self addSubview:self.btnRemove];

    
    NSDictionary * views = @{@"imgView" : self.imgView,
                             @"lblComment" : self.lblComment,
                             @"commentBk":self.commentBk,
                             @"btnRemove" : self.btnRemove};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imgView]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-30-[imgView]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[commentBk]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[commentBk]|" options:0 metrics:nil views:views]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btnRemove attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:3.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btnRemove attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-3.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btnRemove attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:0.0 constant:24.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.btnRemove attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:0.0 constant:24.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.commentBk attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:0.33 constant:-30.0]];
    
    [self.commentBk addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[lblComment]-8-|" options:0 metrics:nil views:views]];
    [self.commentBk addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[lblComment]-8-|" options:0 metrics:nil views:views]];

}

-(void)remove:(id)sender
{
    if([self.delegate respondsToSelector:@selector(requestRemove:)])
    {
        [self.delegate requestRemove:self.bindingContext];
    }
}

-(void)setBindingContext:(id)bindingContext
{
    _bindingContext = bindingContext;
    [self onBindingContextChanged:bindingContext];
    
}

-(void)onBindingContextChanged:(AGCPhotoModel*)model
{
    self.imgView.image = model.image;
    self.lblComment.text = model.comment;
    self.commentBk.hidden = (nil == model.comment || model.comment.length <= 0);
}

@end
