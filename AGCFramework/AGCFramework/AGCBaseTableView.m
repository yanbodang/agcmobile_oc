//
//  AGCBaseTableView.m
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCBaseTableView.h"

@interface AGCBaseTableView()

-(void)additionalInitialization;

@end

@implementation AGCBaseTableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)init
{
    self = [super init];
    [self additionalInitialization];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    [self additionalInitialization];
    
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame: frame style:style];
    [self additionalInitialization];
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self additionalInitialization];

}

#pragma mark - Private Methods;

-(void)additionalInitialization
{
    self.separatorInset = UIEdgeInsetsZero;
    self.layoutMargins = UIEdgeInsetsZero;
    self.bounces = NO;
    
    self.rowHeight = UITableViewAutomaticDimension;
    //remove empty row
    self.tableFooterView = [[UIView alloc] init];
}

@end
