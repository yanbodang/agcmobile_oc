//
//  MFMailComposeViewController+AGC.h
//  AGCFramework
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <MessageUI/MessageUI.h>

@interface MFMailComposeViewController (AGC)

-(UIStatusBarStyle)preferredStatusBarStyle;


@end
