//
//  ISaving.h
//  AGCFramework
//
//  Created by Yanbo Dang on 15/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ISaving <NSObject>

@optional

-(BOOL)save;
-(BOOL)saveModel:(nullable id)model;
-(BOOL)saveModel:(nullable id)model parameter:(id _Nullable)param completedHandler:(void (^_Nullable)(NSError * _Nullable error))completed;

@end
