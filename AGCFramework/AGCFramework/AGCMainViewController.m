//
//  AGCMainViewController.m
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCMainViewController.h"
#import "AGCMainMenuViewController.h"
#import "AGCBaseNavigationController.h"
#import "AGCMainMenuViewController.h"

@interface AGCMainViewController ()

@end

@implementation AGCMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.underLeftViewController = [[AGCBaseNavigationController alloc] initWithRootViewController:[[AGCMainMenuViewController alloc] init]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
