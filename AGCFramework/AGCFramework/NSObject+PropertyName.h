// Release under MIT
// Copyright (C) 2015 Xaree Lee

#import <Foundation/Foundation.h>

/* Get property name for the class (C string or NSSting). */
#define keypathForClass(Klass, PropertyName) \
        (((void)(NO && ((void)[Klass _nullObjectForCheckingPropertyName].PropertyName, NO)), # PropertyName))
#define keypathStringForClass(Klass, PropertyName) \
        @keypathForClass(Klass, PropertyName)

/* Get property name for the protocol (C string or NSSting). */
#define keypathForProtocol(Protocol, PropertyName) \
        (((void)(NO && ((void)((NSObject<Protocol> *)[NSObject _nullObjectForCheckingPropertyName]).PropertyName, NO)), # PropertyName))
#define keypathStringForProtocol(Protocol, PropertyName) \
        @keypathForProtocol(Protocol, PropertyName)


@interface NSObject (PropertyName)
+ (instancetype)_nullObjectForCheckingPropertyName;
@end