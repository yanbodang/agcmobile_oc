//
//  NSArray+AGC.m
//  AGCFramework
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "NSArray+AGC.h"

@implementation NSArray (AGC)

-(NSString *)componentsJoinedByString:(NSString *)separator withFormater:(NSString * (^)(NSString * value))formater
{
    __block NSString * sJoint = @"";
    
    [self enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString * sFormated = formater(obj);
        sJoint = [sJoint stringByAppendingFormat:@"%@%@",sFormated,separator];
    }];
    
    sJoint = [sJoint stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:separator]];
    
    return sJoint;
}

@end
