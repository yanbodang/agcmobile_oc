//
//  AGCBaseCollectionViewCell.h
//  AGCFramework
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGCBaseCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong)id bindingContext;

@end
