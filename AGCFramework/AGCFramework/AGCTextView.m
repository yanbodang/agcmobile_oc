//
//  AGCTextView.m
//  AGCFramework
//
//  Created by Yanbo Dang on 31/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCTextView.h"

@interface AGCTextView()

-(void)applyAGCStyle;

@end

@implementation AGCTextView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)init
{
    self = [super init];
    [self applyAGCStyle];
    return self;
}


-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    [self applyAGCStyle];
    return  self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self applyAGCStyle];
    
    return self;
}

-(void)applyAGCStyle
{
    [self.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.layer setBorderWidth:1.0];
    
    self.layer.cornerRadius = 5;
    self.clipsToBounds = YES;
}

@end
