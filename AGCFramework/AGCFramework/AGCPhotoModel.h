//
//  AGCPhotoModel.h
//  AGCFramework
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseModel.h"

@interface AGCPhotoModel : BaseModel

@property(nonatomic, strong)UIImage * image;
@property(nonatomic, copy)NSString * comment;
@property(nonatomic, assign)BOOL selected;
@property(nonatomic, strong)id rawModel;

-(instancetype)initWithImage:(UIImage*)image comment:(NSString*)comment;

@end
