//
//  AGCPhotoCollectionHeader.h
//  AGCFramework
//
//  Created by Yanbo Dang on 31/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AGCPhotoCollectionHeaderDelegate <NSObject>

@optional

-(void)requestAddPhoto:(id)sender;

@end

@interface AGCPhotoCollectionHeader : UICollectionReusableView

@property(nonatomic, assign)id<AGCPhotoCollectionHeaderDelegate> delegate;

@end
