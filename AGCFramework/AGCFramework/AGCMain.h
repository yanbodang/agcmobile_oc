//
//  AGCMain.h
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AGCMain : NSObject

+(instancetype)shareInstance;

+(UIViewController *)getRootViewController;

+(void)registerHomeViewController:(UIViewController *)homeViewController;

+(void)registerInstanceStoryboard:(UIStoryboard*)storyboard;

-(UIStoryboard*)getMainStoryboard;

@end
