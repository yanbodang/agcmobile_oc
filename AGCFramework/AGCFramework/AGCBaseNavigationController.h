//
//  AGCBaseNavigationController.h
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGCBaseNavigationController : UINavigationController

@property (nonatomic,strong,readonly)UIViewController * rootViewController;

@end
