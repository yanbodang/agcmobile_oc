//
//  AGCHttpClient.m
//  AGCFramework
//
//  Created by Yanbo Dang on 15/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCHttpClient.h"
#import "AFNetworking.h"
#import "AGCHttpClientConfiguration.h"
#import "Constants.h"

@interface AGCHttpClient()

@property (nonatomic, strong)AGCHttpClientConfiguration * config;

+(NSString*)getMimeTypeFromExtension:(NSString *)extension;
-(AFHTTPSessionManager *)getSessionManager;

@end

@implementation AGCHttpClient

#pragma  mark - Initializer

-(instancetype)initWithConfig:(AGCHttpClientConfiguration*)config
{
    self = [super init];
    self.config = config;
    return self;
}

#pragma mark - Private Methods

+(NSString*)getMimeTypeFromExtension:(NSString *)extension
{
    NSString* sMimeType = @"";
    if([extension isEqualToString:@".jpg"] || [extension isEqualToString:@".jpeg"])
        sMimeType = @"image/jpeg";
    
    return sMimeType;
}

-(AFHTTPSessionManager *)getSessionManager
{
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    
    [manager.requestSerializer setValue:@"CAS256" forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:self.config.sUserName password:self.config.sPassword];
    
    return manager;
}

#pragma mark - Public Methods

-(void) getURL:(NSString*)url parameter:(id)param
       success:( void (^)(id respond))success
       failure:( void (^)(NSError* error))failure
{
    
    AFHTTPSessionManager * manager = [self getSessionManager];
    
    NSString * sURL = [NSString stringWithFormat:@"%@%@",self.config.sBaseURL, url];
    
    [manager GET:sURL parameters:param progress:^(NSProgress * _Nonnull downloadProgress){
        return;
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        BLOCK_SAFE_RUN(success, responseObject)
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        BLOCK_SAFE_RUN(failure,error)
    }];
}

-(void) postURL:(NSString*)url parameter:(id)param
        success:(void (^)(id respond)) success
        failure:(void (^)(NSError * error)) failure
{
    AFHTTPSessionManager * manager = [self getSessionManager];
    
    NSString * sURL = [NSString stringWithFormat:@"%@%@",self.config.sBaseURL, url];
    
    [manager POST:sURL parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
        return;
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        BLOCK_SAFE_RUN(success, responseObject)
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        BLOCK_SAFE_RUN(failure, error)
    }];
    
}

-(void)downloadFromUrl:(NSString*)url toPath:(NSString*)path
              progress:(void (^)(double completedPercent)) progress
               success:(void (^)(id response))success
               failure:(void (^)(NSError * error))failure
{
    AFHTTPSessionManager * manager = [self getSessionManager];
    
    NSString * sURL = [NSString stringWithFormat:@"%@%@",self.config.sBaseURL, url];
    
    NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:sURL]];
    
    NSURLSessionDownloadTask * downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        BLOCK_SAFE_RUN(progress,downloadProgress.fractionCompleted)
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        return [NSURL URLWithString:path];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if(nil == error)
        {
            BLOCK_SAFE_RUN(success,response)
        }
        else
        {
            BLOCK_SAFE_RUN(failure,error)
        }
        
    }];
    
    [downloadTask resume];
}

-(void)uploadToUrl:(NSString*)url parameter:(id)param progress:(void (^)(double completedPercent)) progress
     constructData:(NSArray<__kindof AGCHttpFormFile* > * (^)(void))uploadData
           success:(void (^)(id response))success
           failure:(void (^)(NSError* error))failure
{
    AFHTTPSessionManager * manager = [self getSessionManager];
    
    NSString * sURL = [NSString stringWithFormat:@"%@%@",self.config.sBaseURL, url];
    
    
    [manager POST:sURL parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
    {
        NSArray * files = uploadData();
        for(AGCHttpFormFile * file in files)
        {
            [formData appendPartWithFileData:file.data name:file.name fileName:[NSString stringWithFormat:@"%@%@",file.name,file.extension] mimeType:[file getMimeType]];
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"%f",uploadProgress.fractionCompleted);
        BLOCK_SAFE_RUN(progress, uploadProgress.fractionCompleted)
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        BLOCK_SAFE_RUN(success, responseObject)
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        BLOCK_SAFE_RUN(failure, error)
    }];
}

@end


@implementation AGCHttpFormFile

-(NSString*)getMimeType
{
    NSString * mime = @"";
    switch (self.fileType) {
        case  kJpe:
            mime = @"image/jpeg";
            break;
            
        default:
            break;
    }
    
    return mime;
}

@end
