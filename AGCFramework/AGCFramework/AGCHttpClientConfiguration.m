//
//  AGCHttpClientConfiguration.m
//  AGCFramework
//
//  Created by Yanbo Dang on 17/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCHttpClientConfiguration.h"

@interface AGCHttpClientConfiguration()

@end

@implementation AGCHttpClientConfiguration

-(instancetype)initWithBaseURL:(NSString*)baseURL UserName:(NSString*)userName Password:(NSString*)password
{
    self = [super init];
    
    self.sBaseURL = baseURL;
    _sUserName = userName;
    _sPassword = password;
    
    return self;
}

@end
