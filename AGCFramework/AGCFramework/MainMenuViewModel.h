//
//  MainMenuViewModel.h
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "BaseViewModel.h"

@interface MainMenuViewModel : BaseViewModel

@property(nonatomic, assign)BOOL bSelected;
@property(nonatomic, copy)NSString * menuTitle;
@property(nonatomic, copy)NSString * menuIconName;
@property(nonatomic, strong)id (^action)(id);

-(instancetype)initMenuTitle:(NSString*)title menuIcon:(NSString*)iconName;

@end
