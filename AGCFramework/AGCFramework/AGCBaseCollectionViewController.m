//
//  AGCBaseCollectionViewController.m
//  AGCFramework
//
//  Created by Yanbo Dang on 15/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCBaseCollectionViewController.h"
#import "AGCStyle.h"

@interface AGCBaseCollectionViewController ()

-(void)initCollectionView;

@end

@implementation AGCBaseCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;

    // Do any additional setup after loading the view.
    
    [self initCollectionView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initCollectionView
{
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:[self getCollectionViewLayout]];
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [AGCStyle agcWhite];
    [self.view addSubview:self.collectionView];
    
    NSDictionary * views = @{@"collectionView" : self.collectionView,
                             @"view" : self.view,
                             @"topGuide" : self.topLayoutGuide,
                             @"bottomGuide" : self.bottomLayoutGuide};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[topGuide][collectionView][bottomGuide]" options:0 metrics:nil views:views]];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
