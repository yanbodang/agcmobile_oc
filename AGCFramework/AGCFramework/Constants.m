//
//  Constants.m
//  AGCFramework
//
//  Created by Yanbo Dang on 15/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>


#pragma mark - API

/*
 @"http://portal.agcoombs.com.au/MobileTOT017/v1.2/GPSLocation/GPSLocationService.svc";
 @"http://portal.agcoombs.com.au/Mobile/Dispatch/V2.0/DispatchService.svc";
 @"http://10.0.16.10:100/AGC.Dispatch.Wcf/DispatchService.svc";
 @"http://portal.agcoombs.com.au/Mobile/Dispatch/V2.0/DispatchService.svc";
 @"http://10.0.16.14:121/AGC.EmployeePrestart.Wcf/EPService.svc";
 @"http://10.0.16.14:125/AGC.EmployeePrestart.Wcf/EPService.svc";
 */
const NSString* API_WSBASE = @"http://portal.agcoombs.com.au/Mobile/Prestarter/v1.0/EPService.svc";
const NSString* API_GETALLJOBS = @"/GetAllJobs";
const NSString* API_GETALLPREFERENCES = @"/GetAllPreferences";
const NSString* API_POSTDATA_V2 = @"/PostDataV2";

#pragma mark - String Constants

const NSString* SEmpty = @"";
const NSString* SOK = @"OK";
const NSString* SYES = @"YES";
const NSString* SNO = @"NO";
const NSString* SCANCEL = @"Cancel";
const NSString* NA = @"NA";
const NSString* SUCCESS = @"Success";
const NSString* SERROR = @"Error";
const NSString* SAVE = @"Save";
const NSString* SAVED = @"Saved";
const NSString* CLOSE = @"Close";
const NSString* SUBMIT = @"Submit";
