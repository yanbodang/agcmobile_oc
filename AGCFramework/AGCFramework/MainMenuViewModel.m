//
//  MainMenuViewModel.m
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "MainMenuViewModel.h"

@implementation MainMenuViewModel

-(instancetype)initMenuTitle:(NSString*)title menuIcon:(NSString*)iconName
{
    self = [super init];
    
    self.menuTitle = title;
    self.menuIconName = iconName;
    
    return self;
}

@end
