//
//  AGCMainMenuViewController.m
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCMainMenuViewController.h"
#import "AGCBaseTableView.h"
#import "UIImage+Bundle.h"
#import "MainMenuTableViewCell.h"
#import "AGCStyle.h"
#import "MainMenuViewModel.h"
#import "UIViewController+AGCFoundation.h"
#import "AGCMain.h"

#define CELL_ID_MAIN_MENU_CELL @"mainMenuCellId"

@interface AGCMainMenuViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong)AGCBaseTableView * menuTableView;
@property (nonatomic, strong)UILabel * lblVersion;
@property (nonatomic, strong)UILabel * lblCopyright;


-(void)initMenuTable;

@end

@implementation AGCMainMenuViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.bindingContext = [self initBindingContext];
    
    self.menuTableView.estimatedRowHeight = 50;
    
    [self initMenuTable];
    [self enableSideMenuGesture];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    
    NSIndexPath * indexPathForSelRow = [self.menuTableView indexPathForSelectedRow];
    if(nil == indexPathForSelRow && ((NSArray*)self.bindingContext).count > 0)
        [self.menuTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)applyTheme:(id)param
{
    UIImage * image = [UIImage agcResourceImageNamed:@"agcLogo"];
    UIImageView * imageView = [[UIImageView alloc] initWithImage:image];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:imageView];
}

#pragma mark - Private Methods

-(void)initMenuTable
{
    self.menuTableView = [[AGCBaseTableView alloc] init];
    [self.menuTableView registerClass:[MainMenuTableViewCell class] forCellReuseIdentifier:CELL_ID_MAIN_MENU_CELL];
    self.menuTableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.menuTableView.delegate = self;
    self.menuTableView.dataSource = self;
    self.menuTableView.allowsMultipleSelection = NO;
    [self.view addSubview:self.menuTableView];
    self.menuTableView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.9];

    self.lblVersion = [[UILabel alloc] init];
    self.lblVersion.text = @"version:1.0.0 build 123";
    self.lblVersion.textColor = [AGCStyle agcLightBlack];
    self.lblVersion.font = [UIFont systemFontOfSize:12.0];
    self.lblVersion.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.lblCopyright = [[UILabel alloc] init];
    self.lblCopyright.text = @"© Copyright 2017 A.G. Coombs";
    self.lblCopyright.textColor = [AGCStyle agcLightBlack];
    self.lblCopyright.font = [UIFont systemFontOfSize:12.0];
    self.lblCopyright.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.lblCopyright];
    [self.view addSubview:self.lblVersion];
    
    
    NSDictionary * views = @{@"view":self.view,
                             @"menuTableView":self.menuTableView,
                             @"lblVersion" : self.lblVersion,
                             @"lblCopyright" : self.lblCopyright};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[menuTableView]|" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[menuTableView]|" options:0 metrics:nil views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[lblVersion]" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[lblCopyright]" options:0 metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lblVersion]-8-[lblCopyright]-8-|" options:0 metrics:nil views:views]];

    
}

#pragma mark - UITableViewDataSource

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * sectionHeaderView = [[UIView alloc] init];
    sectionHeaderView.backgroundColor = [AGCStyle agcLightBlack];
    
    
    UILabel * lblSectionTitle = [[UILabel alloc] init];
    lblSectionTitle.text = @"Quick Menu";
    lblSectionTitle.textColor = [AGCStyle agcWhite];
    lblSectionTitle.font = [UIFont boldSystemFontOfSize:16.0];
    lblSectionTitle.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIView * underlineView = [[UIView alloc] init];
    underlineView.translatesAutoresizingMaskIntoConstraints = NO;
    underlineView.backgroundColor = [AGCStyle darkGreen];
    underlineView.hidden = YES;
    
    
    [sectionHeaderView addSubview:lblSectionTitle];
    [sectionHeaderView addSubview:underlineView];
    
    [sectionHeaderView addConstraint:[NSLayoutConstraint constraintWithItem:lblSectionTitle attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:sectionHeaderView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:16.0]];
    
    [sectionHeaderView addConstraint:[NSLayoutConstraint constraintWithItem:lblSectionTitle attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:sectionHeaderView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    [sectionHeaderView addConstraint:[NSLayoutConstraint constraintWithItem:underlineView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:lblSectionTitle attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    
    [sectionHeaderView addConstraint:[NSLayoutConstraint constraintWithItem:underlineView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:lblSectionTitle attribute:NSLayoutAttributeBottom multiplier:1.0 constant:4.0]];
    
    [underlineView addConstraint:[NSLayoutConstraint constraintWithItem:underlineView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:0.0 constant:4.0]];
    
    [underlineView addConstraint:[NSLayoutConstraint constraintWithItem:underlineView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:0.0 constant:35.0]];
    
    return sectionHeaderView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [AGCStyle heightMainMenuSectionHeader];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((NSArray*)self.bindingContext).count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainMenuTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID_MAIN_MENU_CELL forIndexPath:indexPath];
    
    cell.bindingContext = [((NSArray*)self.bindingContext) objectAtIndex:indexPath.row];
    
    return cell;
}


#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainMenuViewModel * viewModel = [((NSArray*)self.bindingContext) objectAtIndex:indexPath.row];
    viewModel.bSelected = YES;
    if(nil == viewModel.action)
        return;

    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf updateTopViewController:viewModel.action([[AGCMain shareInstance] getMainStoryboard])];
    });
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainMenuViewModel * viewModel = [((NSArray*)self.bindingContext) objectAtIndex:indexPath.row];
    viewModel.bSelected = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
