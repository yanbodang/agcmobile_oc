//
//  AGCBaseButton.h
//  AGCFramework
//
//  Created by Yanbo Dang on 16/8/17.
//  Copyright © 2017 A.G Coombs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGCBaseButton : UIButton

@property (nonatomic, assign)IBInspectable CGFloat cornerRadius;

@end
