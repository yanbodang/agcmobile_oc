//
//  MFMailComposeViewController+AGC.m
//  AGCFramework
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "MFMailComposeViewController+AGC.h"

@implementation MFMailComposeViewController (AGC)

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
