//
//  UIView+AGC.m
//  AGCFramework
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "UIView+AGC.h"

@implementation UIView (AGC)

-(UIViewController*)parentViewController
{
    UIResponder * responder = self;
    while([responder isKindOfClass:[UIView class]])
        responder = [responder nextResponder];
    return (UIViewController*)responder;
}

@end
