//
//  AGCBaseTabBarController.h
//  AGCFramework
//
//  Created by Yanbo Dang on 18/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBinding.h"
#import "ITheme.h"
#import "ISaving.h"

@interface AGCBaseTabBarController : UITabBarController<ISaving, IBinding, ITheme>

@property(nonatomic, strong)id bindingContext;

@property(nonatomic, strong)NSArray <__kindof UIBarButtonItem *> * leftBarItems;
@property(nonatomic, strong)NSArray <__kindof UIBarButtonItem *> * rightBarItems;

@end
