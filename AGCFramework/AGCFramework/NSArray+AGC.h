//
//  NSArray+AGC.h
//  AGCFramework
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (AGC)

-(NSString *)componentsJoinedByString:(NSString *)separator withFormater:(NSString * (^)(NSString * value))formater;

@end
