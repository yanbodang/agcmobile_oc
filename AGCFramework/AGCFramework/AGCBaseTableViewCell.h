//
//  AGCBaseTableViewCell.h
//  AGCFramework
//
//  Created by Yanbo Dang on 22/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGCBaseTableViewCell : UITableViewCell

@property(nonatomic, strong)id bindingContext;

@end
