//
//  IBinding.h
//  AGCFramework
//
//  Created by Yanbo Dang on 22/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IBinding <NSObject>

@optional

-(id)initBindingContext;

@end
