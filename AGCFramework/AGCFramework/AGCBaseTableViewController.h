//
//  AGCBaseTableViewController.h
//  AGCFramework
//
//  Created by Yanbo Dang on 15/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGCBaseViewController.h"
#import "IAGCTableViewDelegate.h"

@class AGCBaseTableView;

@interface AGCBaseTableViewController : AGCBaseViewController<UITableViewDataSource, UITableViewDelegate,IAGCTableViewDelegate>

@property(nonatomic, strong)AGCBaseTableView * tableView;

@end
