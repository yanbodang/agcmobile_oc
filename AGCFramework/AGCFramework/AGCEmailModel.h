//
//  AGCEmailModel.h
//  AGCFramework
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "BaseModel.h"
#import <MessageUI/MFMessageComposeViewController.h>

@interface AGCEmailModel : BaseModel

@property (nonatomic, copy)NSString * subject;
@property (nonatomic, copy)NSString * message;
@property (nonatomic, copy)NSString * attachmentPath;
@property (nonatomic, copy)NSString * attachmentMimeType;
@property (nonatomic, strong)NSArray * recipients;

@end
