//
//  AGCBaseViewController+AGCFoundation.h
//  AGCFramework
//
//  Created by Yanbo Dang on 22/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISaving.h"
#import "ITheme.h"

@interface UIViewController (AGCFoundation)<ISaving, ITheme>

-(void)enableSideMenuGesture;
-(void)updateTopViewController:(UIViewController*)viewController;

@end
