//
//  AGCMain.m
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCMain.h"
#import "AGCMainViewController.h"
#import "AGCMainMenuViewController.h"
#import "AGCBaseNavigationController.h"
#import "AGCStyle.h"

@interface AGCMain()

@property (nonatomic, strong)AGCMainViewController * rootViewController;
@property (nonatomic, assign)NSArray * (^mainMenuLoader)(void);
@property (nonatomic, strong)UIStoryboard * mainStoryboard;

-(void)buildUpAGCRootViewController;
-(void)applyAGCAppearence;

@end

@implementation AGCMain

-(instancetype)init
{
    self = [super init];
    [self buildUpAGCRootViewController];
    [self applyAGCAppearence];
    return self;
}

-(void)buildUpAGCRootViewController
{
    self.rootViewController = [[AGCMainViewController alloc] init];
}

-(void)applyAGCAppearence
{
    [[UINavigationBar appearance] setBarTintColor:[AGCStyle agcDarkBlack]];
    [[UINavigationBar appearance] setTintColor:[AGCStyle agcWhite]];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[AGCStyle agcWhite]}];
    
}

+(instancetype)shareInstance
{
    static AGCMain * instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[AGCMain alloc] init];
    });
    
    return instance;
}

+(UIViewController *)getRootViewController
{
    return [AGCMain shareInstance].rootViewController;
}

+(void)registerHomeViewController:(UIViewController *)homeViewController
{
    AGCMain * agcMain = [AGCMain shareInstance];
    agcMain.rootViewController.topViewController = homeViewController;
}

+(void)registerInstanceStoryboard:(UIStoryboard*)storyboard
{
    [AGCMain shareInstance].mainStoryboard = storyboard;
}

-(UIStoryboard*)getMainStoryboard
{
    return self.mainStoryboard;
}

@end
