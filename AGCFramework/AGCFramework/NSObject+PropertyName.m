// Release under MIT
// Copyright (C) 2015 Xaree Lee

#import "NSObject+PropertyName.h"

@implementation NSObject (PropertyName)
+ (instancetype)_nullObjectForCheckingPropertyName;{
  return nil;
}
@end