//
//  AGCMainMenuViewController.h
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGCBaseViewController.h"

@interface AGCMainMenuViewController : AGCBaseViewController

@end
