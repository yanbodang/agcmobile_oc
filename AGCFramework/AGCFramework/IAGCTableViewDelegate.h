//
//  IAGCTableViewDelegate.h
//  AGCFramework
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IAGCTableViewDelegate <NSObject>

@optional

-(NSString*)headerTitleForSecion:(NSInteger)secion;

@end
