//
//  AGCPhotoModel.m
//  AGCFramework
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCPhotoModel.h"

@implementation AGCPhotoModel

-(instancetype)initWithImage:(UIImage*)image comment:(NSString*)comment
{
    self = [super init];
    
    self.image = image;
    self.comment = comment;
    
    return self;
}

@end
