//
//  AGCHttpClientConfiguration.h
//  AGCFramework
//
//  Created by Yanbo Dang on 17/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGCHttpClientConfiguration : NSObject

@property(nonatomic, copy)NSString * sBaseURL;
@property(nonatomic, readonly)NSString * sUserName;
@property(nonatomic, readonly)NSString * sPassword;

-(instancetype)initWithBaseURL:(NSString*)baseURL UserName:(NSString*)userName Password:(NSString*)password;

@end
