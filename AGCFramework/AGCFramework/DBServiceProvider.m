//
//  DBServiceProvider.m
//  AGCFramework
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "DBServiceProvider.h"
#import "FMDatabase.h"
#import "Constants.h"
#import "NSArray+AGC.h"
#import "FMDatabaseQueue.h"

@implementation DBServiceProvider

+(NSString *)creatDBWithName:(NSString*)dbName
{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSString * path = [documentDir stringByAppendingPathComponent:dbName];
    FMDatabase * db = [FMDatabase databaseWithPath:path];
    
    if(nil != db)
        return path;
    return nil;
}

+(BOOL)executeSql:(NSString*)sSQL onDB:(NSString * )dbName
{
    FMDatabase * db = [FMDatabase databaseWithPath:dbName];
    BOOL bRslt = NO;
    
    if([db open] && [db beginTransaction])
    {
        
        bRslt = [db executeStatements:sSQL] && [db commit];
        
        if(!bRslt)
            [db rollback];
        
        [db close];
    }
    
    return bRslt;
}

+(BOOL)executeUpdate:(NSString*)sSql onDB:(NSString *)dbName
{
    BOOL bRslt = NO;
    FMDatabase * db = [FMDatabase databaseWithPath:dbName];
    
    if([db open] && [db beginTransaction])
    {
        bRslt = [db executeUpdate:sSql] && [db commit];
        if(!bRslt)
            [db rollback];
        
        [db close];
    }
    
    return bRslt;
}

+(BOOL)executeUpdate:(NSString*)sSql withParam:(NSDictionary*)param onDB:(NSString *)dbName
{
    BOOL bRslt = NO;
    FMDatabase * db = [FMDatabase databaseWithPath:dbName];
    
    if([db open] && [db beginTransaction])
    {
        bRslt = [db executeUpdate:sSql withParameterDictionary:param] && [db commit];
        
        if(!bRslt)
            [db rollback];
        
        [db close];
    }
    
    return bRslt;
}


+(BOOL)executeUpdate:(NSString*)sSql withKeyValuePairs:(NSArray*)pairs onDB:(NSString *)dbName
{
    BOOL bRslt = YES;
    FMDatabase * db = [FMDatabase databaseWithPath:dbName];
    
    if( (bRslt = ([db open] && [db beginTransaction])))
    {
        for(NSDictionary * dic in pairs)
        {
            bRslt = bRslt & [db executeUpdate:sSql withParameterDictionary:dic];
        }
        
        bRslt = bRslt & [db commit];
        
        if(!bRslt)
            [db rollback];
        
        [db close];
    }
    
    return bRslt;
}

+(BOOL)querySql:(NSString*)sSQL onDB:(NSString * )dbName queryResult:(void (^)(id result, BOOL finished))result
{
    FMDatabase * db = [FMDatabase databaseWithPath:dbName];
    BOOL bRslt = NO;
    
    if( (bRslt = [db open]))
    {
        
        FMResultSet * rs = [db executeQuery:sSQL];
        while([rs next])
        {
            id resultDic = rs.resultDictionary;
            BLOCK_SAFE_RUN(result, resultDic,NO)
        }
        BLOCK_SAFE_RUN(result, nil, YES)
        [db close];
    }
    
    return bRslt;
}


+(BOOL)insertOrReplaceOnTable:(NSString*)tableName withKeyValuePair:(NSDictionary*)pair onDB:(NSString*)dbName
{
    __block BOOL bRslt = NO;
    
    FMDatabaseQueue * dbQueue = [FMDatabaseQueue databaseQueueWithPath:dbName];
    [dbQueue inDatabase:^(FMDatabase * _Nonnull db) {
        
        NSString * sColumns = [[pair allKeys] componentsJoinedByString:@","];
        NSString * sValues = [[pair allKeys] componentsJoinedByString:@"," withFormater:^NSString *(NSString *value) {
            return [NSString stringWithFormat:@":%@",value];
        }];
        
        NSString * sSql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@(%@) VALUES(%@)",tableName,sColumns,sValues];
        
        if( (bRslt = [db open]))
        {
            bRslt =  [db executeUpdate:sSql withParameterDictionary:pair];
            
            [db close];
        }
    }];
    return bRslt;
}

+(BOOL)insertOrReplaceOnTable:(NSString*)tableName withKeyValuePairs:(NSArray*)pairs onDB:(NSString*)dbName
{
    __block BOOL bRslt = YES;
    
    FMDatabaseQueue * dbQueue = [FMDatabaseQueue databaseQueueWithPath:dbName];
    
    [dbQueue inDatabase:^(FMDatabase * _Nonnull db) {
        
        if( (bRslt = ([db open] && [db beginTransaction])))
        {
            for(NSDictionary * dic in pairs)
            {
                NSString * sColumns = [[dic allKeys] componentsJoinedByString:@","];
                NSString * sValues = [[dic allKeys] componentsJoinedByString:@"," withFormater:^NSString *(NSString *value) {
                    return [NSString stringWithFormat:@":%@",value];
                }];
                
                NSString * sSql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@(%@) VALUES(%@)",tableName,sColumns,sValues];
                bRslt = bRslt && [db executeUpdate:sSql withParameterDictionary:dic];
                if(!bRslt)
                {
                    [db rollback];
                    break;
                }
            }
            bRslt = bRslt && [db commit];
            if(!bRslt)
                [db rollback];
            
            [db close];
        }
    }];
    

    return bRslt;
}

@end
