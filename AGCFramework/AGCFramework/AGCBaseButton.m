//
//  AGCBaseButton.m
//  AGCFramework
//
//  Created by Yanbo Dang on 16/8/17.
//  Copyright © 2017 A.G Coombs. All rights reserved.
//

#import "AGCBaseButton.h"

@interface AGCBaseButton()

-(void)setup;

@end

@implementation AGCBaseButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)init
{
    self = [super init];
    
    [self setup];
    
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    [self setup];
    
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    return self;
}


-(void)setup
{
    //self.layer.cornerRadius = self.cornerRadius;
}

-(void)setCornerRadius:(CGFloat)cornerRadius
{
    _cornerRadius = cornerRadius;
    self.layer.cornerRadius = self.cornerRadius;
}

@end
