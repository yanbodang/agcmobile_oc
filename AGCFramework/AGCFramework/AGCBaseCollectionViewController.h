//
//  AGCBaseCollectionViewController.h
//  AGCFramework
//
//  Created by Yanbo Dang on 15/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGCBaseViewController.h"


@interface AGCBaseCollectionViewController : AGCBaseViewController <UICollectionViewDataSource,UICollectionViewDelegate>

@property(nonatomic, strong)UICollectionView * collectionView;

-(UICollectionViewCell*)getCollectionViewLayout;

@end
