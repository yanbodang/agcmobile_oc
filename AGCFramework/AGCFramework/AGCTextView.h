//
//  AGCTextView.h
//  AGCFramework
//
//  Created by Yanbo Dang on 31/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGCTextView : UITextView

@end
