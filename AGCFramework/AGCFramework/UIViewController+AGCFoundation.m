//
//  AGCBaseViewController+AGCFoundation.m
//  AGCFramework
//
//  Created by Yanbo Dang on 22/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "UIViewController+AGCFoundation.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIImage+Bundle.h"

@interface UIViewController (Theme)

-(void)attachSideMenuBarItem:(id)param;
-(void)triggeSideMenu:(id)sender;

@end

@implementation UIViewController (Theme)

-(void)attachSideMenuBarItem:(id)param
{
    UIImage * image = [UIImage agcResourceImageNamed:@"sideMenuIconNormal"];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(triggeSideMenu:)];
}

-(void)triggeSideMenu:(id)sender
{
    if(self.slidingViewController.currentTopViewPosition == ECSlidingViewControllerTopViewPositionCentered)
    {
        [self.slidingViewController anchorTopViewToRightAnimated:YES];
    }
    else
    {
        [self.slidingViewController resetTopViewAnimated:YES];
    }
}

@end

@implementation UIViewController (AGCFoundation)

-(void)enableSideMenuGesture
{
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
}

-(void)updateTopViewController:(UIViewController*)viewController
{
    self.slidingViewController.topViewController = viewController;
    [self.slidingViewController resetTopViewAnimated:YES];
}

/*

#pragma mark - ISaving

-(void)saveModel:(nullable id)model
{
    
}
-(void)saveModel:(nullable id)model parameter:(id _Nullable)param completedHandler:(void (^_Nullable)(NSError * _Nullable error))completed
{
    
}
*/

#pragma mark - ITheme

-(void)applyTheme:(id)param
{
    [self attachSideMenuBarItem:param];
}

@end
