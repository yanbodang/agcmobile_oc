//
//  AGCBaseTabBarController.m
//  AGCFramework
//
//  Created by Yanbo Dang on 18/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCBaseTabBarController.h"
#import "AGCBaseViewController.h"

@interface AGCBaseTabBarController ()

-(void)addRightBarItems:(NSArray *)items;

-(void)addLeftBarItems:(NSArray *)items;

@end

@implementation AGCBaseTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self applyTheme:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    

    [self addLeftBarItems:self.leftBarItems];
    [self addRightBarItems:self.rightBarItems];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)addRightBarItems:(NSArray *)items
{
    self.navigationItem.rightBarButtonItems = items;
}

-(void)addLeftBarItems:(NSArray *)items
{
    self.navigationItem.leftBarButtonItems = items;
}


@end
