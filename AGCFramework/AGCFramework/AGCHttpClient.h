//
//  AGCHttpClient.h
//  AGCFramework
//
//  Created by Yanbo Dang on 15/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AGCHttpClientConfiguration;

typedef NS_ENUM(NSInteger, AGCHttpFormFileType)
{
    kJpe = 0,
};



@interface AGCHttpFormFile : NSObject

@property(nonatomic, strong)NSData* data;
@property(nonatomic, copy)NSString* name;
@property(nonatomic, copy)NSString* extension;
@property(nonatomic, assign)AGCHttpFormFileType fileType;

-(NSString*)getMimeType;

@end



@interface AGCHttpClient : NSObject

-(instancetype)initWithConfig:(AGCHttpClientConfiguration*)config;

-(void) getURL:(NSString*)url parameter:(id)param
       success:( void (^)(id respond))success
       failure:( void (^)(NSError* error))failure;

-(void) postURL:(NSString*)url parameter:(id)param
        success:(void (^)(id respond)) success
        failure:(void (^)(NSError * error)) failure;


-(void)downloadFromUrl:(NSString*)url toPath:(NSString*)path
              progress:(void (^)(double completedPercent)) progress
               success:(void (^)(id response))success
               failure:(void (^)(NSError * error))failure;

-(void)uploadToUrl:(NSString*)url parameter:(id)param progress:(void (^)(double completedPercent)) progress
            constructData:(NSArray<__kindof AGCHttpFormFile* > * (^)(void))uploadData
            success:(void (^)(id response))success
            failure:(void (^)(NSError* error))failure;

@end
