//
//  UIImage+Bundle.h
//  AGCFramework
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Bundle)

+(instancetype)imageNamed:(NSString*)sImageName inBundleNamed:(NSString*)bundleName;

+(instancetype)agcResourceImageNamed:(NSString*)sImageName;

@end
