//
//  EmployeeModel.m
//  EPS
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EmployeeModel.h"
#import "JobModel.h"

@implementation EmployeeModel

-(instancetype)initWithName:(NSString*)name company:(NSString*)company job:(JobModel*)job
{
    self = [super init];
    
    self.Name = name;
    self.Company = company;
    self.JobNumber = job.JobNumber;
    self.SiteForemanEmail = job.SiteForemanEmail;
    self.ActiveFlag = 1;
    
    return self;
}

@end
