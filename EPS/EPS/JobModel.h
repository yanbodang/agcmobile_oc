//
//  JobModel.h
//  EPS
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>

@interface JobModel : BaseModel

@property (nonatomic, copy) NSString * AdditionalRecipient;
@property (nonatomic, copy) NSString * JobNumber;
@property (nonatomic, copy) NSString * Name;
@property (nonatomic, copy) NSString * PassKey;
@property (nonatomic, copy) NSString * Path;
@property (nonatomic, copy) NSString * ProjectAdmin;
@property (nonatomic, copy) NSString *SiteForeman;
@property (nonatomic, copy) NSString *SiteForemanEmail;
@property (nonatomic, copy) NSString *SiteForemanTitle;
@property (nonatomic, copy) NSString *SiteLocation;
@property (nonatomic, assign) int DefaultFlag;


@end
