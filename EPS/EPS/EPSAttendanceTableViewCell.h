//
//  EPSAttendanceTableViewCell.h
//  EPS
//
//  Created by Yanbo Dang on 31/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>

@protocol EPSAttendanceTableViewCellDelegate <NSObject>

@optional

-(void)requestChangeTime:(id)sender;

@end


@interface EPSAttendanceTableViewCell : AGCBaseTableViewCell

@property(nonatomic, assign)id<EPSAttendanceTableViewCellDelegate> delegate;

@property(nonatomic, copy)NSString * dateTime;

@end
