//
//  EPSFormDetailView.m
//  EPS
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSFormDetailView.h"
#import "FormModel.h"
#import "JobModel.h"
#import "JobViewModel.h"
#import "FormViewModel.h"

@interface EPSFormDetailView()<LineDrawnDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *txtProjectName;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *txtSite;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *txtJobNumber;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *txtConductedName;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *txtConductedTitle;
@property (weak, nonatomic) IBOutlet SignatureView *imgSignagure;
@property (weak, nonatomic) IBOutlet UITextView *txtSafteyComment;
@property (weak, nonatomic) IBOutlet UITextView *txtDailyActivites;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segStaftyComment;


-(void)onBindingContextChanged:(FormModel*)model;

@end


@implementation EPSFormDetailView

@synthesize bindingContext = _bindingContext;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.imgSignagure.delegate = self;
}

-(void)setBindingContext:(id)bindingContext
{
    _bindingContext = bindingContext;
    
    [self onBindingContextChanged:((FormViewModel*)bindingContext).form];
}

-(void)onBindingContextChanged:(FormModel*)model
{
    self.txtSafteyComment.text = model.SafetyConcernDescription;
    self.txtDailyActivites.text = model.WorkActivity;
    

    if ([model.SafetyConcernFlag isEqualToString: SYES] )
    {
        self.segStaftyComment.selectedSegmentIndex = 0;
    }
    else
    {
        self.segStaftyComment.selectedSegmentIndex = 1;
    }

    self.txtProjectName.text  = model.JobName;
    self.txtJobNumber.text = model.JobNumber;
    self.txtConductedName.text= model.SiteForeman;
    self.txtSite.text= model.SiteLocation;
    self.txtConductedTitle.text = model.SiteForemanTitle;
    self.lblDate.text = model.Date;
    
    if(nil != model.Signature)
        self.imgSignagure.image = [UIImage imageWithData:model.Signature];
}

-(BOOL)save
{
    
    FormModel * form = ((FormViewModel*)self.bindingContext).form;
    form.JobName = self.txtProjectName.text;
    form.JobNumber = self.txtJobNumber.text;
    form.SiteForeman = self.txtConductedName.text;
    form.SiteLocation = self.txtSite.text;
    form.SiteForemanTitle = self.txtConductedTitle.text;
    if(0 == self.segStaftyComment.selectedSegmentIndex)
        form.SafetyConcernFlag = @"YES";
    else if(1 == self.segStaftyComment.selectedSegmentIndex)
        form.SafetyConcernFlag = @"NO";
    
    form.SafetyConcernDescription = self.txtSafteyComment.text;
    form.WorkActivity = self.txtDailyActivites.text;
    form.CompletedFlag = @"";
    form.Signature = UIImageJPEGRepresentation(self.imgSignagure.image, 1.0);
    if(nil == form.Signature) form.Signature = UIImageJPEGRepresentation([UIImage imageNamed:@"imgWhiteBox"],1.0);
    
    return [self.bindingContext save];
    
}


#pragma mark - LineDrawnDelegate

-(void)startDrawn
{
    if([self.delegate respondsToSelector:@selector(requestDisableScroll:)])
    {
        [self.delegate requestDisableScroll:YES];
    }
}

-(void)lineDrawn
{
    if([self.delegate respondsToSelector:@selector(requestDisableScroll:)])
    {
        [self.delegate requestDisableScroll:NO];
    }
}

@end
