//
//  FormPhotoModel.h
//  EPS
//
//  Created by Yanbo Dang on 4/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>

@class FormModel;

@interface FormPhotoModel : BaseModel

@property (nonatomic, copy) NSString *PhotoID;
@property (nonatomic, copy) NSString *Description;
@property (nonatomic, copy) NSString *SiteForemanEmail;
@property (nonatomic, copy) NSString *Date;
@property (nonatomic, strong) NSData *ImageData;
@property (nonatomic, copy) NSString *SortOrder;


@end
