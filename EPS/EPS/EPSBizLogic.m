//
//  EPSBizLogic.m
//  EPS
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSBizLogic.h"
#import "Common.h"
#import "JobViewModel.h"
#import <AGCFramework/AGCFramework.h>


@interface EPSBizLogic()

-(BOOL)createTables;
-(AGCHttpClient *)createHttpClient;

@end

@implementation EPSBizLogic

+(instancetype)shareInstance
{
    static EPSBizLogic * biz = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        biz = [[EPSBizLogic alloc] init];
    });
    
    return biz;
}

-(BOOL)initEPSDataBase
{
    BOOL bRslt = NO;
    
    _sDBName = [DBServiceProvider creatDBWithName:DB_NAME];
    if(nil != self.sDBName)
        bRslt = [self createTables];

    return bRslt;
}

-(BOOL)hasDefaultJob
{
    return _hasDefaultJob = [JobViewModel getDefaultJob] != nil;
}

#pragma mark - Private Methods

-(AGCHttpClient *)httpClient
{
    if(nil == _httpClient)
        _httpClient = [self createHttpClient];
    
    return _httpClient;
}


-(AGCHttpClient *)createHttpClient
{
    AGCHttpClientConfiguration * cfg = [[AGCHttpClientConfiguration alloc] initWithBaseURL:API_WSBASE UserName:WSUSER_NAME Password:WSPASSWORD];
    return [[AGCHttpClient alloc] initWithConfig:cfg];
}

-(BOOL)createTables
{
    NSString * sSql = @""
    "CREATE TABLE IF NOT EXISTS Employee ( "
    "Name TEXT, "
    "SiteForemanEmail TEXT, "
    "JobNumber TEXT, "
    "Company TEXT, "
    "ActiveFlag INT, "
    "PRIMARY KEY (Name, JobNumber, SiteForemanEmail));"
    
    "CREATE TABLE IF NOT EXISTS Job ( "
    "Name TEXT, "
    "JobNumber TEXT, "
    "SiteForeman TEXT, "
    "SiteForemanEmail TEXT, "
    "SiteForemanTitle TEXT, "
    "SiteLocation TEXT, "
    "PassKey TEXT, "
    "DefaultFlag INT, "
    "ProjectAdmin TEXT, "
    "Path TEXT, "
    "AdditionalRecipient TEXT, "
    "PRIMARY KEY (JobNumber, SiteForemanEmail));"
    
    "CREATE TABLE IF NOT EXISTS Form ( "
    "JobNumber TEXT, "
    "SiteForemanEmail TEXT, "
    "Date TEXT, "
    "Signature BLOB, "
    "SafetyConcernFlag TEXT, "
    "SafetyConcernDescription TEXT,"
    "WorkActivity TEXT, "
    "CompletedFlag TEXT, "
    "JobName  TEXT, "
    "SiteForeman  TEXT, "
    "SiteLocation  TEXT, "
    "SiteForemanTitle  TEXT, "
    "PRIMARY KEY (JobNumber, SiteForemanEmail, Date));"
    
    "CREATE TABLE IF NOT EXISTS Preference ( "
    "PreferenceName TEXT, "
    "Category TEXT, "
    "Code TEXT, "
    "Value TEXT, "
    "PRIMARY KEY (PreferenceName));"
    
    "CREATE TABLE IF NOT EXISTS FormCheckList ( "
    "JobNumber TEXT, "
    "SiteForemanEmail TEXT, "
    "Date TEXT, "
    "PreferenceName TEXT, "
    "PreferenceCode TEXT, "
    "SortOrder TEXT, "
    "Item TEXT, "
    "Respond TEXT, "
    "Comment TEXT, "
    "PRIMARY KEY (JobNumber, SiteForemanEmail, Date, PreferenceName));"
    
    "CREATE TABLE IF NOT EXISTS FormAttendance ( "
    "JobNumber TEXT, "
    "SiteForemanEmail TEXT, "
    "Date TEXT, "
    "EmployeeName TEXT, "
    "Attendance TEXT, "
    "Comment TEXT, "
    "Company TEXT, "
    "PRIMARY KEY (JobNumber, SiteForemanEmail, Date, EmployeeName));"
    
    "CREATE TABLE IF NOT EXISTS FormPhoto ("
    "JobNumber TEXT, "
    "SiteForemanEmail TEXT, "
    "Date TEXT, "
    "PhotoID TEXT, "
    "ImageData BLOB, "
    "Description TEXT, "
    "SortOrder TEXT, "
    "PRIMARY KEY (PhotoID))";
    
    return [DBServiceProvider executeSql:sSql onDB:self.sDBName];
}

@end
