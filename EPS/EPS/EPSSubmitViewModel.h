//
//  EPSSubmitViewModel.h
//  EPS
//
//  Created by Yanbo Dang on 5/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSBaseViewModel.h"

@interface EPSSubmitViewModel : EPSBaseViewModel

@property(nonatomic, strong)NSArray * emails;

@property(nonatomic, copy)void (^ submitProgres)(double progress);

-(void)appendEmail:(NSString*)email;
-(void)removeEmail:(NSString*)email;
-(NSString*)toEmailsRecipients;


@end
