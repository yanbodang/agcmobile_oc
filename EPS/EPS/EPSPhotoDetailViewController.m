//
//  EPSPhotoDetailViewController.m
//  EPS
//
//  Created by Yanbo Dang on 4/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSPhotoDetailViewController.h"
#import "EPSPhotoDetailViewModel.h"
#import "EPSPhotoDetailView.h"
#import "MBProgressHUD.h"

@interface EPSPhotoDetailViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) EPSPhotoDetailView * detailView;

-(void)save:(id)sender;

-(void)back:(id)sender;

@end

@implementation EPSPhotoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createLeftBarItems];
    [self createRightBarItems];
    
    self.detailView = [[[NSBundle mainBundle] loadNibNamed:@"EPSPhotoDetailView" owner:self options:nil] objectAtIndex:0];
    self.detailView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.scrollView addSubview:self.detailView];
    
    [self.scrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.detailView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    
    [self.scrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.detailView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.detailView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.detailView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0]];
    /*
    [self.scrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.detailView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    

    
    [self.scrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.detailView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
     */
    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.scrollView.contentSize = self.view.frame.size;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    EPSPhotoDetailViewModel * vm = (EPSPhotoDetailViewModel*)self.bindingContext;
    self.detailView.bindingContext = vm;
}


-(void)createRightBarItems
{
    UIBarButtonItem * btnSave = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconSaveWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(save:)];
    
    self.rightBarItems = @[btnSave];
}


-(void)createLeftBarItems
{
    UIBarButtonItem * btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage agcResourceImageNamed:@"iconNaviBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    
    self.leftBarItems = @[btnBack];
}

-(void)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)save:(id)sender
{
    if([self.detailView save])
    {
        MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Saved Successfully";
        [hud hide:YES afterDelay:1.0];
    }
}

-(BOOL)saveModel:(id)model
{
    return [self.detailView save];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
