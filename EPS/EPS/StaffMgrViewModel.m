//
//  StaffMgrViewModel.m
//  EPS
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "StaffMgrViewModel.h"
#import "JobViewModel.h"
#import "JobViewModel.h"
#import "JobModel.h"
#import "EmployeeModel.h"
#import "MJExtension.h"
#import <AGCFramework/AGCFramework.h>

@interface StaffMgrViewModel()

@end

@implementation StaffMgrViewModel

-(void)initViewModelWithParam:(id)param completed:(void (^)(id))completedHandle
{
    JobModel * job = [JobViewModel getDefaultJob];
    
    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM Employee WHERE SiteForemanEmail = '%@' AND JobNumber = '%@' ORDER BY Name ASC", job.SiteForemanEmail, job.JobNumber];
    
    __block NSMutableArray * records = [[NSMutableArray alloc] init];
    [DBServiceProvider querySql:sSql onDB:self.biz.sDBName queryResult:^(id result, BOOL finished) {
         if(!finished)
         {
             [records addObject:[EmployeeModel mj_objectWithKeyValues:result]];
         }
    }];
    
    self.employees = records;
}

-(BOOL)removeEmployee:(EmployeeModel *)employee
{
    BOOL bRslt = NO;
    
    NSString * sSql = [NSString stringWithFormat:@"DELETE FROM Employee WHERE Name = '%@' AND SiteForemanEmail = '%@' AND JobNumber = '%@' ", employee.Name, employee.SiteForemanEmail, employee.JobNumber];

    
    if( (bRslt = [DBServiceProvider executeUpdate:sSql onDB:self.biz.sDBName]))
    {
        NSMutableArray * records = (NSMutableArray*)self.employees;
        [records removeObject:employee];
        self.employees = records;
    }
    
    return bRslt;
}

-(BOOL)addEmployeeName:(NSString*)name company:(NSString*)company
{
    BOOL bRslt = NO;
    
    JobModel * job = [JobViewModel getDefaultJob];
    
    EmployeeModel * employee = [[EmployeeModel alloc] initWithName:name company:company job:job];
    
    if( (bRslt = [DBServiceProvider insertOrReplaceOnTable:@"Employee" withKeyValuePair:[employee mj_keyValues] onDB:self.biz.sDBName]))
    {
        NSMutableArray * records = (NSMutableArray*)self.employees;
        [records addObject:employee];
        self.employees = records;
    }

    return bRslt;
}

@end
