//
//  EPSSubmitViewController.m
//  EPS
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSSubmitViewController.h"
#import "EPSSubmitViewModel.h"
#import "EPSEmialTableViewCell.h"
#import "FormViewModel.h"
#import "EPSFormTabBarController.h"
#import "MBProgressHUD.h"

static NSString * CELL_ID_EMAIL = @"EPSEmialTableViewCell";

@interface EPSSubmitViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet AGCBaseTableView *tableView;

-(void)createRightBarItems;

-(void)addEmailRecipient:(id)sender;
-(void)submit:(id)sender;

@end

@implementation EPSSubmitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createRightBarItems];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"EPSEmialTableViewCell" bundle:nil] forCellReuseIdentifier:CELL_ID_EMAIL];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    EPSSubmitViewModel * vm = [[EPSSubmitViewModel alloc] init];
    [vm initViewModelWithParam:((FormViewModel*)((EPSFormTabBarController*)self.tabBarController).bindingContext).form completed:nil];
    
    self.bindingContext = vm;
    
    self.leftBarItems = ((EPSFormTabBarController*)self.tabBarController).leftBarItems;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)createRightBarItems
{
    UIBarButtonItem * btnSubmit = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconSubmitMenuWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(submit:)];

    self.rightBarItems = @[btnSubmit];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)submit:(id)sender
{
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Please wait...";
    hud.detailsLabelText = @"You will get the confirmation email once this message disappears.";
    hud.userInteractionEnabled = NO;
    hud.dimBackground = true;
    
    __weak typeof(self) weakSelf = self;
    
    ((EPSSubmitViewModel*)self.bindingContext).submitProgres = ^(double progress) {
        
        long percent = (long)(progress * 100);
        if(percent >= 100)
        {
            hud.labelText = [NSString stringWithFormat:@"Upload finised, please wait for completed..."];
        }
        else
        {
            hud.labelText = [NSString stringWithFormat:@"Please wait, %ld%% completed",percent];
        }
    };
    
    [self.bindingContext saveModel:nil parameter:nil completedHandler:^(NSError * _Nullable error) {
        
        BOOL bSuccess = nil == error;
        
        if(nil == error)
        {
            hud.labelText = @"Submit Successfully";
        }
        else if(nil != error)
        {
            
            [AGCUtility showAlert:@"Oops" message:[error.userInfo objectForKey:@"ErrMsg"] onController:self defaultAction:nil defaultActionTitle:@"OK"];
        }
        else
        {
            hud.labelText = @"Submit Failed";
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [hud hide:YES afterDelay:1.0];
            
            if(bSuccess)
                [weakSelf.navigationController popViewControllerAnimated:YES];
        });
    }];
}


-(void)addEmailRecipient:(id)sender
{
    UIAlertController * alertCtrl = [UIAlertController alertControllerWithTitle:@"" message:@"Add an additional email address" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertCtrl addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        textField.placeholder = @"email address";
    }];
    
    __weak typeof(self) weakSelf = self;
    
    UIAlertAction * confirmAction = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString * sEmail = [alertCtrl.textFields objectAtIndex:0].text;
        sEmail = [sEmail stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if(nil != sEmail & sEmail.length > 0)
        {
            if([AGCUtility verifyEmail:sEmail])
            {
                [(EPSSubmitViewModel*)weakSelf.bindingContext appendEmail:sEmail];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [weakSelf.tableView reloadData];
                    [alertCtrl dismissViewControllerAnimated:YES completion:nil];
                });
            }
        }
        
    }];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alertCtrl addAction:confirmAction];
    [alertCtrl addAction:cancelAction];
    
    [self presentViewController:alertCtrl animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((EPSSubmitViewModel*)self.bindingContext).emails.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EPSEmialTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID_EMAIL forIndexPath:indexPath];
    
    cell.bindingContext = [((EPSSubmitViewModel*)self.bindingContext).emails objectAtIndex:indexPath.row];
    
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * header = [[UIView alloc] init];
    header.backgroundColor = [AGCStyle agcLightBlack];
    
    UILabel * lblTitle = [[UILabel alloc] init];
    lblTitle.text = @"Distribution email list";
    lblTitle.textColor = [AGCStyle agcWhite];
    lblTitle.font = [UIFont boldSystemFontOfSize:16.0];
    lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIButton * btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAdd.translatesAutoresizingMaskIntoConstraints = NO;
    [btnAdd setBackgroundImage:[UIImage imageNamed:@"iconAddWhite"] forState:UIControlStateNormal];
    [btnAdd addTarget:self action:@selector(addEmailRecipient:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView * vDecorated = [[UIView alloc] init];
    vDecorated.translatesAutoresizingMaskIntoConstraints = NO;
    vDecorated.backgroundColor = [AGCStyle darkOringe];
    
    [header addSubview:lblTitle];
    [header addSubview:btnAdd];
    [header addSubview:vDecorated];
    
    NSDictionary * views = @{@"lblTitle" : lblTitle,
                             @"btnAdd" : btnAdd,
                             @"vDecorated" : vDecorated,
                             @"header" : header};
    
    [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[vDecorated(4)]-8-[lblTitle]" options:0 metrics:nil views:views]];
    [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnAdd(24)]-8-|" options:0 metrics:nil views:views]];
    
    [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[vDecorated]|" options:0 metrics:nil views:views]];
    
    [header addConstraint:[NSLayoutConstraint constraintWithItem:lblTitle attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:header attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    [header addConstraint:[NSLayoutConstraint constraintWithItem:btnAdd attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:header attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    [header addConstraint:[NSLayoutConstraint constraintWithItem:btnAdd attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:0.0 constant:24.0]];
    
    return header;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [AGCStyle heightMainMenuSectionHeader];
}

#pragma  mark - UITableVeiwDelegate


-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(UITableViewCellEditingStyleDelete == editingStyle)
    {
        NSString * email = [((EPSSubmitViewModel*)self.bindingContext).emails objectAtIndex:indexPath.row];
        
        [(EPSSubmitViewModel*)self.bindingContext removeEmail:email];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}


@end
