//
//  EPSFormTableViewCell.m
//  EPS
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSFormTableViewCell.h"
#import "FormModel.h"

@interface EPSFormTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *lblFormDate;
@property (weak, nonatomic) IBOutlet UILabel *lblFormTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewTicked;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

-(void)onUpdateBindingContext:(FormModel*)formModel;

@end

@implementation EPSFormTableViewCell

@synthesize bindingContext = _bindingContext;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setBindingContext:(id)bindingContext
{
    _bindingContext = bindingContext;
    [self onUpdateBindingContext:(FormModel*)bindingContext];
    
}

-(void)onUpdateBindingContext:(FormModel*)formModel
{
    self.lblFormTitle.text = formModel.JobName;
    if([formModel.CompletedFlag isEqualToString: SYES])
    {
        self.lblStatus.text = @"STATUS:SUBMITTED";
        self.lblStatus.textColor = [AGCStyle darkOringe];
        self.imgViewTicked.image = [UIImage imageNamed:@"iconTicked"];
    }
    else
    {
        self.lblStatus.text = @"STATUS: NOT SUBMITTED";
        self.lblStatus.textColor = [AGCStyle agcLightBlack];
        self.imgViewTicked.image = [UIImage imageNamed:@"iconUnsubmitted"];
    }
    

    // convert to date
    NSDate * date = [AGCUtility dataFromString:formModel.Date format:@"YYYY-MM-dd"];
    self.lblFormDate.text = [AGCUtility date:date format:@"dd EE, MMM yyyy"];
    
    NSCalendar *calender = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    if([calender isDateInWeekend:date])
    {
        self.backgroundColor = [UIColor colorWithRed:(231/255.0) green:(235/255.0) blue:(240/255.0) alpha:1];
    }
    else
        self.backgroundColor = [AGCStyle agcWhite];

}

@end
