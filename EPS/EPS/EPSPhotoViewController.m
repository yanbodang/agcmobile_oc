//
//  EPSPhotoViewController.m
//  EPS
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSPhotoViewController.h"
#import <Photos/Photos.h>
#import "MWPhotoBrowser.h"
#import "EPSPhotoCollectionViewModel.h"
#import "EPSFormTabBarController.h"
#import "EPSPhotoDetailViewController.h"
#import "EPSPhotoDetailViewModel.h"
#import "MBProgressHUD.h"
#import "FormViewModel.h"

@interface EPSPhotoViewController ()<AGCPhotoCollectionHeaderDelegate, MWPhotoBrowserDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic, weak)id curSelectedCellModel;

-(void)requestTakePhoto;

-(void)takePhoto;

-(void)pickupPhoto;

-(void)browserPhotosAlbum;

@end

@implementation EPSPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    EPSPhotoCollectionViewModel * vm = [[EPSPhotoCollectionViewModel alloc] init];
    id param = ((FormViewModel*)((EPSFormTabBarController*)self.tabBarController).bindingContext).form;
    
    
    __weak typeof(self) weakSelf = self;
    
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading photo...";
    [hud show:YES];
    
    [vm initViewModelWithParam:param completed:^(id data) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            weakSelf.bindingContext = data;
            [weakSelf.collectionView reloadData];
            
            [hud hide:YES];
        });
    }];
    
    self.leftBarItems = ((EPSFormTabBarController*)self.tabBarController).leftBarItems;
    self.rightBarItems = ((EPSFormTabBarController*)self.tabBarController).rightBarItems;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.collectionView reloadData];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)requestTakePhoto
{
    __weak typeof(self) weakSelf = self;
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(AVAuthorizationStatusNotDetermined == authStatus)
    {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf takePhoto];
                });
            }
            else
            {
                UIAlertController * alertCtrl = [UIAlertController alertControllerWithTitle:@"Pre-Start" message:@"You didn't give the permission to access your camear." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                
                [alertCtrl addAction:action];
                
                [weakSelf presentViewController:alertCtrl animated:YES completion:nil];
            }
        }];
    }
    else if(AVAuthorizationStatusAuthorized == authStatus)
    {
        [self takePhoto];
    }
    else
    {
        UIAlertController * alertCtrl = [UIAlertController alertControllerWithTitle:@"Pre-Start" message:@"Please assign the permission for access your camera" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alertCtrl addAction:action];
        
        [self presentViewController:alertCtrl animated:YES completion:nil];
    }
}

-(void)takePhoto
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)pickupPhoto
{
    
    __weak typeof(self) weakSelf = self;
    PHAuthorizationStatus authStatus = [PHPhotoLibrary authorizationStatus];
    if(PHAuthorizationStatusNotDetermined == authStatus)
    {
        
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if(PHAuthorizationStatusAuthorized == status)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf browserPhotosAlbum];
                });
            }
            else
            {
                UIAlertController * alertCtrl = [UIAlertController alertControllerWithTitle:@"Pre-Start" message:@"You didn't give the permission to access your photo library." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                
                [alertCtrl addAction:action];
                
                [weakSelf presentViewController:alertCtrl animated:YES completion:nil];
            }
        }];
    }
    else if(PHAuthorizationStatusAuthorized == authStatus)
    {
        [self browserPhotosAlbum];
    }
    else
    {
        UIAlertController * alertCtrl = [UIAlertController alertControllerWithTitle:@"Pre-Start" message:@"Please assign the permission for access your photo library" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        
        [alertCtrl addAction:action];
        
        [self presentViewController:alertCtrl animated:YES completion:nil];
    }
}

-(void)browserPhotosAlbum
{
    ((EPSPhotoCollectionViewModel *)self.bindingContext).assets = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
    
    MWPhotoBrowser * photoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    photoBrowser.enableGrid = YES;
    photoBrowser.startOnGrid = YES;
    photoBrowser.displayActionButton = NO;
    photoBrowser.displaySelectionButtons = YES;
    
    [self.navigationController pushViewController:photoBrowser animated:YES];
}

#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    AGCPhotoCollectionCell * cell = (AGCPhotoCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    self.curSelectedCellModel = cell.bindingContext;
    
    [self performSegueWithIdentifier:@"segueShowPhotoDetail" sender:self];
}


#pragma mark - AGCPhotoCollectionHeaderDelegate

-(void)requestAddPhoto:(id)sender
{
    __weak typeof(self) weakSelf = self;
    UIAlertAction * alertTakePhoto = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf requestTakePhoto];
    }];
    
    UIAlertAction * alertPickupPhoto = [UIAlertAction actionWithTitle:@"Pick up photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf pickupPhoto];
    }];
    
    UIAlertAction * alertCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alertController addAction:alertTakePhoto];
    [alertController addAction:alertPickupPhoto];
    [alertController addAction:alertCancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - AGCPhotoCollectionCellDelegate

-(void)requestRemove:(id)model
{
    __weak typeof(self) weakSelf = self;
    UIAlertAction * alertPositive = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            if([(EPSPhotoCollectionViewModel*)weakSelf.bindingContext removePhoto:model])
            {
                [weakSelf.collectionView reloadData];
            }
        });
       
    }];
    
    UIAlertAction * alertCancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:@"are you sure to remove this picture?" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:alertPositive];
    [alertController addAction:alertCancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - MWPhotoBrowserDelegate

-(NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return ((EPSPhotoCollectionViewModel *)self.bindingContext).assets.count;
}
-(id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    if(index >= ((EPSPhotoCollectionViewModel *)self.bindingContext).assets.count)
        return nil;
    PHAsset * asset = [((EPSPhotoCollectionViewModel *)self.bindingContext).assets objectAtIndex:index];
    CGSize size = CGSizeMake(asset.pixelWidth, asset.pixelHeight);
    MWPhoto * photo = [MWPhoto photoWithAsset: asset targetSize:size];
    return photo;
}

-(id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index
{
    if(index >= ((EPSPhotoCollectionViewModel *)self.bindingContext).assets.count)
        return nil;
    PHAsset * asset = [((EPSPhotoCollectionViewModel *)self.bindingContext).assets objectAtIndex:index];
    MWPhoto * photo = [MWPhoto photoWithAsset: asset targetSize:CGSizeMake(150.0, 150.0)];
    return photo;
}

-(BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index
{
    return [(EPSPhotoCollectionViewModel *)self.bindingContext hasSelectedAsset:index];
}

-(void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected
{
    if(selected)
        [(EPSPhotoCollectionViewModel *)self.bindingContext selectAssetAtIndex:index];
    else
        [(EPSPhotoCollectionViewModel *)self.bindingContext deselectAssetAtIndex:index];
}

#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage * img = info[UIImagePickerControllerEditedImage];
    
    if([(EPSPhotoCollectionViewModel*)self.bindingContext appendPhoto:img comment:@""])
    {
        [self.collectionView reloadData];
    }

    [picker dismissViewControllerAnimated:YES completion:NULL];

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UIStoryboard

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"segueShowPhotoDetail"])
    {
        EPSPhotoDetailViewController * vc = (EPSPhotoDetailViewController *)segue.destinationViewController;
        EPSPhotoDetailViewModel * vm = [[EPSPhotoDetailViewModel alloc] init];
        [vm initViewModelWithParam:self.curSelectedCellModel completed:nil];
        
        vc.bindingContext = vm;
    }
}

@end
