//
//  EPSQuestionTableViewCell.m
//  EPS
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSQuestionTableViewCell.h"
#import "FormChecklistModel.h"

@interface EPSQuestionTableViewCell()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtComment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segCtrl;

- (IBAction)onSegSelected:(id)sender;

-(void)onUpdateBindingContext:(FormChecklistModel*)model;

@end

@implementation EPSQuestionTableViewCell

@synthesize bindingContext = _bindingContext;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.txtComment.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.txtComment.layer setBorderWidth:1.0];
    
    self.txtComment.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setBindingContext:(id)bindingContext
{
    _bindingContext = bindingContext;
    [self onUpdateBindingContext:(FormChecklistModel*)bindingContext];
    
}

- (IBAction)onSegSelected:(id)sender {
    
    NSString *answer  = SEmpty;
    UISegmentedControl * seg = (UISegmentedControl*)sender;
    
    if(seg.selectedSegmentIndex == 0) {
        answer = SYES;
    }
    else if(seg.selectedSegmentIndex == 1) {
        answer = SNO;
    }
    else if(seg.selectedSegmentIndex == 2) {
        answer = NA;
    }
    
    FormChecklistModel* model = (FormChecklistModel*)self.bindingContext;
    model.Respond = answer;
}

-(void)onUpdateBindingContext:(FormChecklistModel *)model
{
    self.txtComment.text = model.Comment;
    self.segCtrl.selectedSegmentIndex = -1;
    self.lblTitle.text = [NSString stringWithFormat:@"%@. %@.", model.PreferenceCode, model.Item];
    
    if([model.Respond isEqualToString:SYES]) {
        self.segCtrl.selectedSegmentIndex = 0;
    }
    
    else if([model.Respond isEqualToString:SNO]) {
        self.segCtrl.selectedSegmentIndex = 1;
    }
    
    else if([model.Respond isEqualToString:NA]) {
        self.segCtrl.selectedSegmentIndex = 2;
    }
}

#pragma mark - UITextViewDelegate

-(void)textViewDidEndEditing:(UITextView *)textView
{
    ((FormChecklistModel*)self.bindingContext).Comment = textView.text;
}

@end
