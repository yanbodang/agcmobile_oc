//
//  FormChecklistModel.h
//  EPS
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>


@class JobModel;
@class PreferenceModel;

@interface FormChecklistModel : BaseModel

@property (nonatomic, copy) NSString *JobNumber;
@property (nonatomic, copy) NSString *SiteForemanEmail;
@property (nonatomic, copy) NSString *Date;
@property (nonatomic, copy) NSString *PreferenceName;
@property (nonatomic, copy) NSString *PreferenceCode;
@property (nonatomic, copy) NSString *SortOrder;
@property (nonatomic, copy) NSString *Item;
@property (nonatomic, copy) NSString *Respond;
@property (nonatomic, copy) NSString *Comment;

-(instancetype)initWithJob:(JobModel*)job Preference:(PreferenceModel*)pref date:(NSString*)date;

@end
