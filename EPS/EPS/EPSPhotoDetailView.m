//
//  EPSPhotoDetailView.m
//  EPS
//
//  Created by Yanbo Dang on 4/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSPhotoDetailView.h"
#import "EPSPhotoDetailViewModel.h"

@interface EPSPhotoDetailView()

@property (weak, nonatomic) IBOutlet UIImageView *img;

@property (weak, nonatomic) IBOutlet AGCTextView *txtComment;

-(void)onBindingContextChanged:(EPSPhotoDetailViewModel*)model;

@end

@implementation EPSPhotoDetailView

@synthesize bindingContext = _bindingContext;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setBindingContext:(id)bindingContext
{
    _bindingContext = bindingContext;
    [self onBindingContextChanged:bindingContext];
}

-(void)onBindingContextChanged:(EPSPhotoDetailViewModel *)model
{
    self.img.image = model.image;
    self.txtComment.text = model.comment;
}

-(BOOL)save
{
    ((EPSPhotoDetailViewModel*)self.bindingContext).comment = self.txtComment.text;
    return [self.bindingContext save];
}


@end
