//
//  EPSDateTimeView.h
//  EPS
//
//  Created by Yanbo Dang on 1/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>

@interface EPSDateTimeView : AGCBaseView

@property(nonatomic, readonly,assign)BOOL shouldApplyAll;
@property(nonatomic,copy)NSString * dateTime;

@property(nonatomic, copy)void (^ confirmHandel)(NSString * dateTime, BOOL bApplyAll);
@property(nonatomic, copy)void (^ cancelHandel)(void);

@end
