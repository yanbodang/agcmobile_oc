//
//  EPSDataManagerViewController.m
//  EPS
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSDataManagerViewController.h"
#import "Common.h"
#import "EPSDataManagerViewModel.h"
#import "MBProgressHUD.h"

@interface EPSDataManagerViewController ()

- (IBAction)download:(id)sender;

@end

@implementation EPSDataManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = TITLE_DATA_SETUP;
    self.bindingContext = [[EPSDataManagerViewModel alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)download:(id)sender {
    
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.labelText = @"Download...";
    
    [(EPSDataManagerViewModel *)self.bindingContext downloadDataWithCompleted:^(BOOL success) {
        if(success)
            hud.labelText = @"Download successfully";
        else
            hud.labelText = @"Download failed";
        
        [hud hide:YES afterDelay:1.0];
    }];
}
@end
