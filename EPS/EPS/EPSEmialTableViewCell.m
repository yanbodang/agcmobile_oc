//
//  EPSEmialTableViewCell.m
//  EPS
//
//  Created by Yanbo Dang on 31/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSEmialTableViewCell.h"

@interface EPSEmialTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *lblEmail;

-(void)onBindingContextChanged:(NSString*)email;

@end

@implementation EPSEmialTableViewCell

@synthesize bindingContext = _bindingContext;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setBindingContext:(id)bindingContext
{
    _bindingContext = bindingContext;
    [self onBindingContextChanged:bindingContext];
}

-(void)onBindingContextChanged:(NSString*)email
{
    self.lblEmail.text = email;
}

@end
