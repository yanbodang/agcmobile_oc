//
//  EPSStaffTableViewCell.m
//  EPS
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSStaffTableViewCell.h"
#import "EmployeeModel.h"

@interface EPSStaffTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *lblStaffInfo;

-(void)onUpdateBindingContext:(EmployeeModel*)employee;

@end

@implementation EPSStaffTableViewCell

@synthesize bindingContext = _bindingContext;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setBindingContext:(id)bindingContext
{
    _bindingContext = bindingContext;
    [self onUpdateBindingContext:(EmployeeModel*)bindingContext];
    
}

-(void)onUpdateBindingContext:(EmployeeModel*)employee
{
    self.lblStaffInfo.text = [NSString stringWithFormat:@"%@ (%@)", employee.Name, employee.Company];
}

@end
