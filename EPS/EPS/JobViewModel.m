//
//  JobViewModel.m
//  EPS
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "JobViewModel.h"
#import "JobModel.h"
#import "MJExtension.h"
#import "Common.h"
#import <AGCFramework/AGCFramework.h>


@implementation JobViewModel

-(instancetype)init
{
    self = [super init];
    
    return self;
}

+(void)getAllJobsWithCompleted:(void (^)(NSError * error))completed;
{
    AGCHttpClient * httpClient = [EPSBizLogic shareInstance].httpClient;
    
    [httpClient getURL:API_GETALLJOBS parameter:nil success:^(id respond) {
        
        NSDictionary * dic = (NSDictionary*)respond;
        
        for (NSString* key in dic)
        {
            NSArray * values  = [dic objectForKey:key];
            [DBServiceProvider insertOrReplaceOnTable:@"Job" withKeyValuePairs:values onDB:[EPSBizLogic shareInstance].sDBName];
        }
        
        
    } failure:^(NSError *error) {
        
    }];
}


+(JobModel*)getJobByJobNumber:(NSString *)jobNumber
{
    __block JobModel * model = nil;
    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM JOB WHERE JobNumber = '%@' LIMIT 1",jobNumber];
    
    [DBServiceProvider querySql:sSql onDB:[EPSBizLogic shareInstance].sDBName queryResult:^(id result, BOOL finished) {
        if(!finished)
        {
            NSDictionary * dic = (NSDictionary*)result;
            model = [JobModel mj_objectWithKeyValues:dic];
        }
    }];
    
    return model;
}

+(BOOL)validJobNumber:(NSString*)jobNumber
{
    __block BOOL hasRecord = NO;
    NSString * sSql = [NSString stringWithFormat:@"SELECT COUNT(*) AS C FROM JOB WHERE JobNumber = '%@'",jobNumber];
    
    [DBServiceProvider querySql:sSql onDB:[EPSBizLogic shareInstance].sDBName queryResult:^(id result, BOOL finished) {
        if(!finished)
        {
            NSDictionary * dic = (NSDictionary*)result;
            NSNumber * number = [dic objectForKey:@"C"];
            hasRecord = [number integerValue] > 0;
        }
    }];
    
    return hasRecord;
}

+(JobModel*)getDefaultJob
{
    JobModel * job = [JobViewModel getJobByJobNumber:[AGCUtility getUserDefaultValueForKey:KEY_JOB_NUMBER]];
    if(nil != job)
        job.SiteForemanEmail = [AGCUtility getUserDefaultValueForKey:KEY_SITE_FOREMAN_EMAIL];
    return job;
}

@end
