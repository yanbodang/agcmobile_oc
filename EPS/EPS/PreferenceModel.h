//
//  PreferenceModel.h
//  EPS
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>

@interface PreferenceModel : NSObject

@property (nonatomic, copy) NSString *PreferenceName;
@property (nonatomic, copy) NSString *Category;
@property (nonatomic, copy) NSString *Code;
@property (nonatomic, copy) NSString *Value;

@end
