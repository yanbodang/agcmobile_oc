//
//  FormModel.h
//  EPS
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>

@class JobModel;

@interface FormModel : BaseModel


@property (nonatomic, copy) NSString *JobName;
@property (nonatomic, copy) NSString *SiteForeman;
@property (nonatomic, copy) NSString *SiteLocation;
@property (nonatomic, copy) NSString *SiteForemanTitle;
@property (nonatomic, copy) NSString *JobNumber;
@property (nonatomic, copy) NSString *SiteForemanEmail;
@property (nonatomic, copy) NSString *Date;
@property (nonatomic, copy) NSString *SafetyConcernFlag;
@property (nonatomic, copy) NSString *SafetyConcernDescription;
@property (nonatomic, copy) NSString *WorkActivity;
@property (nonatomic, copy) NSString *CompletedFlag;
@property (nonatomic, strong) NSData *Signature;


-(instancetype)initWitJob:(JobModel*)job date:(NSString*)date;

@end
