//
//  FormCheckListViewModel.h
//  EPS
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSBaseViewModel.h"

@class FormModel;

@interface FormCheckListViewModel : EPSBaseViewModel

@property(nonatomic, readonly)NSUInteger count;

-(id)getCheckListAt:(NSUInteger)index;

@end
