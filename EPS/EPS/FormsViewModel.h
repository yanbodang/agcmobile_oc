//
//  FormsViewModel.h
//  EPS
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSBaseViewModel.h"

@class FormModel;

@interface FormsViewModel : EPSBaseViewModel

@property(nonatomic, strong,getter=getModels)NSArray * models;
//@property(nonatomic, assign,readonly)BOOL hasForms;

+(FormModel*)getFormByJobNum:(NSString*)jobNumber SiteForemanEmail:(NSString*)email date:(NSString*)date;

+(BOOL)updateForm:(FormModel*)formModel;

-(void)reload;

@end
