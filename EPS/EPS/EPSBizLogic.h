//
//  EPSBizLogic.h
//  EPS
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AGCHttpClient;

@interface EPSBizLogic : NSObject

@property(nonatomic,readonly)NSString * sDBName;
@property(nonatomic,strong)AGCHttpClient * httpClient;
@property(nonatomic, assign)BOOL hasDefaultJob;

+(instancetype)shareInstance;

-(BOOL)initEPSDataBase;




@end
