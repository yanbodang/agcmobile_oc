//
//  EPSHomeTableViewController.h
//  EPS
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCBaseTableViewController.h>

@interface EPSHomeTableViewController : AGCBaseTableViewController

@end
