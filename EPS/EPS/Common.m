//
//  Common.m
//  EPS
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <Foundation/Foundation.h>


const NSString * DB_NAME = @"dbEPS.db";

#pragma mark - Title

const NSString * TITLE_PRESTART_FORM = @"PreStart Forms";
const NSString * TITLE_WORK_INSTRUCTION = @"Work Instruction";
const NSString * TITLE_STAFF_MANAGEMENT = @"Staff Management";
const NSString * TITLE_DATA_SETUP = @"Data Setup";
const NSString * TITLE_JOB_SETUP = @"Job Setup";

#pragma mark - API

const NSString * API_WSBASE = @"http://portal.agcoombs.com.au/Mobile/Prestarter/v1.0/EPService.svc";
const NSString * API_GETALLJOBS = @"/GetAllJobs";
const NSString * API_GETALLPREFERENCES = @"/GetAllPreferences";
const NSString * API_POSTDATA_V2 = @"/PostDataV2";

#pragma mark - String Constants

const NSString* WSUSER_NAME = @"AGC";
const NSString* WSPASSWORD = @"YRrH7XT7v572E6bskNTgebvyzp2wVrGDsramHtcGD8kpbN2MLzqdzQvEKgRWz47";
const NSString* ESP = @"ESP";


#pragma mark - KEY Constants

const NSString* KEY_DIST_EMAIL_LST = @"DistributionEmailList";
const NSString* KEY_SEL_FORM_DATE = @"SelectedFormDate";
const NSString* KEY_JOB_NUMBER = @"JobNumber";
const NSString* KEY_SITE_FOREMAN_EMAIL = @"SiteForemanEmail";
const NSString* KEY_PASSKEY = @"PassKey";
const NSString* KEY_SESSIONID = @"SessionID";
const NSString* KEY_APPVER = @"AppVersion";
const NSString* KEY_SESSION_DATE = @"SessionDate";
const NSString* KEY_SAFETY_CONCERN_FLAG = @"SafetyConcernFlag";
const NSString* KEY_SAFETY_CONCERN_DESC = @"SafetyConcernDescription";
const NSString* KEY_WORK_ACTIVITY = @"WorkActivity";
const NSString* KEY_COMPLETED_FLAG = @"CompletedFlag";
const NSString* KEY_JOB_NAME = @"JobName";
const NSString* KEY_SITE_LOC = @"SiteLocation";
const NSString* KEY_DATE = @"Date";
const NSString* KEY_SITE_FOREMAN_TITLE = @"SiteForemanTitle";
const NSString* KEY_SITE_FOREMAN = @"SiteForeman";
const NSString* KEY_EMAIL_LIST = @"EmailList";
const NSString* KEY_PHOTO_ARR = @"PhotosArray";
const NSString* KEY_FORM = @"Form";
const NSString* KEY_FORM_TYPE = @"FormType";
const NSString* KEY_QA_ARR = @"QuestionAnswersArray";
const NSString* KEY_ATTENDNC_ARR = @"AttendanceArray";
