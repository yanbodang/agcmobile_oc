//
//  AGCPhotoModel+EPS.h
//  EPS
//
//  Created by Yanbo Dang on 4/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>

@class FormPhotoModel;

@interface AGCPhotoModel (EPS)

-(instancetype)initWithFormPhotoModel:(FormPhotoModel*)model;

@end
