//
//  AGCMainMenuViewController+Instance.m
//  EPS
//
//  Created by Yanbo Dang on 22/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCMainMenuViewController+Instance.h"
#import <AGCFramework/MainMenuViewModel.h>

@implementation AGCMainMenuViewController (Instance)

-(id)initBindingContext
{
    static NSArray * menuItems = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        MainMenuViewModel * menuStartForm = [[MainMenuViewModel alloc] initMenuTitle:@"Start Form" menuIcon:@"menuStartForm"];
        MainMenuViewModel * menuMgrStaff = [[MainMenuViewModel alloc] initMenuTitle:@"Manage Staff" menuIcon:@"menuMgrStaff"];
        MainMenuViewModel * menuDownloadData = [[MainMenuViewModel alloc] initMenuTitle:@"Download Data" menuIcon:@"menuDownloadData"];
        MainMenuViewModel * menuWorkInstruction = [[MainMenuViewModel alloc] initMenuTitle:@"Work Instruction" menuIcon:@"menuWorkInstruction"];
        MainMenuViewModel * menuJobSetup = [[MainMenuViewModel alloc] initMenuTitle:@"Job Setup" menuIcon:@"menuJobSetup"];
        
        
        menuStartForm.action = ^id(id param) {
            return [self getViewControllerInStoryboard:(UIStoryboard*)param withIdentifier:@"FormNavigationController"];
        };
        
        menuMgrStaff.action = ^id(id param) {
            return [self getViewControllerInStoryboard:(UIStoryboard*)param withIdentifier:@"ManageEmployeeNavigationController"];
        };
        
        menuDownloadData.action = ^id(id param) {
            return [self getViewControllerInStoryboard:(UIStoryboard*)param withIdentifier:@"DataSetupNavigationController"];
        };
        
        menuWorkInstruction.action = ^id(id param){
            return [self getViewControllerInStoryboard:(UIStoryboard*)param withIdentifier:@"WorkInstructionNavigationController"];
        };
        
        menuJobSetup.action = ^id(id param){
            return [self getViewControllerInStoryboard:(UIStoryboard*)param withIdentifier:@"JobSetupNavigationController"];
        };

        
        menuStartForm.bSelected = YES;
        
        menuItems = @[menuStartForm,
                      menuMgrStaff,
                      menuDownloadData,
                      menuWorkInstruction,
                      menuJobSetup,];
        
    });
    
    return menuItems;
}

-(UIViewController*)getViewControllerInStoryboard:(UIStoryboard*)storyboard withIdentifier:(NSString*)identifier
{
    UIViewController * viewController = nil;
    if(nil != storyboard)
        viewController = [storyboard instantiateViewControllerWithIdentifier:identifier];
    return viewController;
}

@end
