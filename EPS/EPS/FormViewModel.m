//
//  FormViewModel.m
//  EPS
//
//  Created by Yanbo Dang on 5/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "FormViewModel.h"
#import "FormModel.h"
#import "MJExtension.h"

@implementation FormViewModel

-(void)initViewModelWithParam:(id)param completed:(void (^)(id))completedHandle
{
    self.form = (FormModel*)param;
}

-(BOOL)save
{
    return [DBServiceProvider insertOrReplaceOnTable:@"Form" withKeyValuePair:[self.form mj_keyValues] onDB:self.biz.sDBName];
}

@end
