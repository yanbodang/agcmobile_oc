//
//  EPSChecklistViewController.m
//  EPS
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSChecklistViewController.h"
#import "FormCheckListViewModel.h"
#import "EPSQuestionTableViewCell.h"
#import "EPSFormTabBarController.h"
#import "FormViewModel.h"

#define CELL_ID @"EPSFormChecklistTableViewCell"


@interface EPSChecklistViewController ()

@end

@implementation EPSChecklistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    FormCheckListViewModel * vm = [[FormCheckListViewModel alloc] init];
    [vm initViewModelWithParam:((FormViewModel*)((EPSFormTabBarController*)self.tabBarController).bindingContext).form completed:nil];
    
    self.bindingContext = vm;
    self.tableView.estimatedRowHeight = 165;
    [self.tableView registerNib:[UINib nibWithNibName:@"EPSQuestionTableViewCell" bundle:nil] forCellReuseIdentifier:CELL_ID];
    
    self.leftBarItems = ((EPSFormTabBarController*)self.tabBarController).leftBarItems;
    self.rightBarItems = ((EPSFormTabBarController*)self.tabBarController).rightBarItems;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSString*)headerTitleForSecion:(NSInteger)secion
{
    return @"SAFETY CHECK LIST";
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return ((FormCheckListViewModel*)self.bindingContext).count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EPSQuestionTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID forIndexPath:indexPath];
    
    cell.bindingContext = [((FormCheckListViewModel *)self.bindingContext) getCheckListAt:indexPath.row];
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
