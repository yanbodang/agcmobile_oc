//
//  FormAttendanceModel.m
//  EPS
//
//  Created by Yanbo Dang on 31/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "FormAttendanceModel.h"
#import "JobModel.h"
#import "EmployeeModel.h"
#import "FormModel.h"

@implementation FormAttendanceModel


-(instancetype)initWithJob:(JobModel*)job employee:(EmployeeModel*)employee form:(FormModel*)form
{
    self = [super init];
    
    self.JobNumber = job.JobNumber;
    self.SiteForemanEmail = job.SiteForemanEmail;
    self.Date = form.Date;
    self.EmployeeName = employee.Name;
    self.Comment = @"6.45AM";
    self.Company = employee.Company;
    
    return self;
}

@end
