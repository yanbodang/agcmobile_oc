//
//  EPSWorkInstructionViewController.m
//  EPS
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSWorkInstructionViewController.h"
#import "Common.h"

@interface EPSWorkInstructionViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webViewInstruction;

@end

@implementation EPSWorkInstructionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = TITLE_WORK_INSTRUCTION;

    NSString* path = [[NSBundle mainBundle] pathForResource:@"WorkInstruction" ofType:@"pdf"];
    NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]];
    
    [self.webViewInstruction setScalesPageToFit:YES];
    [self.webViewInstruction loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
