//
//  EPSPhotoView.m
//  EPS
//
//  Created by Yanbo Dang on 31/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSPhotoView.h"

@interface EPSPhotoView()

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIView *commentBkView;
@property (weak, nonatomic) IBOutlet UILabel *lblComment;

@end

@implementation EPSPhotoView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
