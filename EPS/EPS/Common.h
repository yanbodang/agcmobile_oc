//
//  Common.h
//  EPS
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#ifndef EPS_Common_h
#define EPS_Common_h

extern NSString * DB_NAME;

#pragma mark - Title

extern NSString * TITLE_PRESTART_FORM;
extern NSString * TITLE_WORK_INSTRUCTION;
extern NSString * TITLE_STAFF_MANAGEMENT;
extern NSString * TITLE_DATA_SETUP;
extern NSString * TITLE_JOB_SETUP;

#pragma mark - API

extern NSString* API_WSBASE;
extern NSString* API_GETALLJOBS;
extern NSString* API_GETALLPREFERENCES;
extern NSString* API_POSTDATA_V2;

#pragma mark - String Constants

extern NSString* WSUSER_NAME;
extern NSString* WSPASSWORD;
extern NSString* ESP;

#pragma mark - KEY Constants

extern NSString* KEY_DIST_EMAIL_LST;
extern NSString* KEY_SEL_FORM_DATE;
extern NSString* KEY_JOB_NUMBER;
extern NSString* KEY_SITE_FOREMAN_EMAIL;
extern NSString* KEY_PASSKEY;
extern NSString* KEY_SESSIONID;
extern NSString* KEY_APPVER;
extern NSString* KEY_SESSION_DATE;
extern NSString* KEY_SAFETY_CONCERN_FLAG;
extern NSString* KEY_SAFETY_CONCERN_DESC;
extern NSString* KEY_WORK_ACTIVITY;
extern NSString* KEY_COMPLETED_FLAG;
extern NSString* KEY_JOB_NAME;
extern NSString* KEY_SITE_LOC;
extern NSString* KEY_DATE;
extern NSString* KEY_SITE_FOREMAN_TITLE;
extern NSString* KEY_SITE_FOREMAN;
extern NSString* KEY_EMAIL_LIST;
extern NSString* KEY_PHOTO_ARR;
extern NSString* KEY_FORM;
extern NSString* KEY_FORM_TYPE;
extern NSString* KEY_QA_ARR;
extern NSString* KEY_ATTENDNC_ARR;





#endif /* EPS_Common_h */
