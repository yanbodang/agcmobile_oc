//
//  FormModel.m
//  EPS
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "FormModel.h"
#import "JobModel.h"

@implementation FormModel


-(instancetype)initWitJob:(JobModel*)job date:(NSString*)date
{
    self = [super init];
    self.JobNumber = job.JobNumber;
    self.SiteForemanEmail = job.SiteForemanEmail;
    self.Date = date;
    self.JobName = job.Name;
    return self;
}

@end
