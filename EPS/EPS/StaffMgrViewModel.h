//
//  StaffMgrViewModel.h
//  EPS
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSBaseViewModel.h"

@class EmployeeModel;

@interface StaffMgrViewModel : EPSBaseViewModel

@property(nonatomic, strong)NSArray * employees;

-(BOOL)removeEmployee:(EmployeeModel *)employee;
-(BOOL)addEmployeeName:(NSString*)name company:(NSString*)company;

@end
