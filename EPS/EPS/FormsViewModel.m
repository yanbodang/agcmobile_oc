//
//  FormsViewModel.m
//  EPS
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//


#import "FormsViewModel.h"
#import "JobViewModel.h"
#import "JobModel.h"
#import "FormModel.h"
#import "MJExtension.h"
#import "Common.h"

@interface FormsViewModel()

@property(nonatomic, strong,getter=getDefaultJob)JobModel * defaultJob;

-(NSArray*)getFormsByJobNum:(NSString*)jobNumber SiteForemanEmail:(NSString*)email;

@end

@implementation FormsViewModel

-(instancetype)init
{
    self = [super init];
    return self;
}

#pragma mark - Private Methods

-(void)initViewModelWithParam:(id)param completed:(void (^)(id))completedHandle
{
    for(int addedDay = -14; addedDay <= 3; ++addedDay)
    {
        NSDate * date = [[NSDate date] dateByAddingTimeInterval: 60 * 60 * 24 * addedDay];
        NSString * sDate = [AGCUtility date:date format:@"yyyy-MM-dd"];
        FormModel * model = [FormsViewModel getFormByJobNum:self.defaultJob.JobNumber SiteForemanEmail:self.defaultJob.SiteForemanEmail date:sDate];
        if(nil == model)
        {
            model = [[FormModel alloc] initWitJob:self.defaultJob date:sDate];
            [FormsViewModel updateForm:model];
        }
    }

    return;
}

-(NSArray*)getFormsByJobNum:(NSString*)jobNumber SiteForemanEmail:(NSString*)email;
{
    __block NSMutableArray * models = [[NSMutableArray alloc] init];
    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM Form WHERE JobNumber = '%@' AND SiteForemanEmail = '%@' ORDER BY DATE DESC LIMIT 17",jobNumber,email];
    
    [DBServiceProvider querySql:sSql onDB:self.biz.sDBName queryResult:^(id result, BOOL finished) {
        if(!finished)
        {
            [models addObject:[FormModel mj_objectWithKeyValues:result]];
        }
    }];
    
    return models;
}

-(NSArray*)getModels
{
    if(nil == _models)
        _models = [self getFormsByJobNum:self.defaultJob.JobNumber SiteForemanEmail:self.defaultJob.SiteForemanEmail];
    
    return _models;
}

-(void)reload
{
    self.models = [self getFormsByJobNum:self.defaultJob.JobNumber SiteForemanEmail:self.defaultJob.SiteForemanEmail];
}

-(JobModel*)getDefaultJob
{
    if(nil == _defaultJob)
        _defaultJob = [JobViewModel getDefaultJob];
    
    return _defaultJob;
}


#pragma mark - Public Methods

+(FormModel*)getFormByJobNum:(NSString*)jobNumber SiteForemanEmail:(NSString*)email date:(NSString*)date
{
    __block FormModel * model = nil;
    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM Form WHERE JobNumber = '%@' AND siteForemanEmail = '%@' AND Date = '%@' ",jobNumber,email,date];
    
    [DBServiceProvider querySql:sSql onDB:[EPSBizLogic shareInstance].sDBName queryResult:^(id result, BOOL finished) {
        if(!finished)
        {
            NSDictionary * dic = (NSDictionary*)result;
            model = [FormModel mj_objectWithKeyValues:dic];
        }

    }];
    
    return model;
}


+(BOOL)updateForm:(FormModel*)formModel
{
    return [DBServiceProvider insertOrReplaceOnTable:@"Form" withKeyValuePair:formModel.mj_keyValues onDB:[EPSBizLogic shareInstance].sDBName];
}

@end
