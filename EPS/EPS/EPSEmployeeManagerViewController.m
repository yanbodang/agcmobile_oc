//
//  EPSEmployeeManagerViewController.m
//  EPS
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSEmployeeManagerViewController.h"
#import "Common.h"
#import "EPSStaffTableViewCell.h"
#import "StaffMgrViewModel.h"

#define CELL_ID @"EPSStaffTableViewCell"

@interface EPSEmployeeManagerViewController ()

-(void)addStaff:(id)sender;

-(void)shakeview:(UIView*)view;

@end

@implementation EPSEmployeeManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = TITLE_STAFF_MANAGEMENT;
    
    self.tableView.estimatedRowHeight = 44;
    [self.tableView registerNib:[UINib nibWithNibName:@"EPSStaffTableViewCell" bundle:nil] forCellReuseIdentifier:CELL_ID];
    
    StaffMgrViewModel * vm = [[StaffMgrViewModel alloc] init];
    [vm initViewModelWithParam:nil completed:nil];
    
    self.bindingContext = vm;
    
    self.rightBarItems = [self getRightBarItems];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSArray*)getRightBarItems
{
    UIBarButtonItem * btnAddStaff = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconAddWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(addStaff:)];
    
    return @[btnAddStaff];
}

-(NSString*)headerTitleForSecion:(NSInteger)secion
{
    return @"Manage your staff";
}

-(void)addStaff:(id)sender
{
    __weak typeof(self) weakSelf = self;
    
    UIAlertController * alertCtrl = [UIAlertController alertControllerWithTitle:@"New Employee" message:@"Please enter full name of the employee" preferredStyle:UIAlertControllerStyleAlert];
    [alertCtrl addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Full Name";
    }];
    
    [alertCtrl addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Company / Employer";
    }];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:SCANCEL style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction * confirmAction = [UIAlertAction actionWithTitle:SOK style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString* sName = [alertCtrl textFields][0].text;
        NSString* sCmp = [alertCtrl textFields][1].text;
        if(sName.length <= 0)
        {
            //[weakSelf shakeview:alertCtrl.view];
            return;
        }
        
        if([(StaffMgrViewModel*)self.bindingContext addEmployeeName:sName company:sCmp])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.tableView reloadData];
            });
        }
        
    }];
    
    [alertCtrl addAction:cancelAction];
    [alertCtrl addAction:confirmAction];
    
    [self presentViewController:alertCtrl animated:YES completion:nil];
}

-(void)shakeview:(UIView*)view
{
    CGFloat t = 2.0;
    CGAffineTransform leftQuake  = CGAffineTransformTranslate(CGAffineTransformIdentity, t, 0);
    CGAffineTransform rightQuake = CGAffineTransformTranslate(CGAffineTransformIdentity, -t, 0);
    
    view.transform = leftQuake;  // starting point
    
    [UIView beginAnimations:@"earthquake" context:(__bridge void *)(view)];
    [UIView setAnimationRepeatAutoreverses:YES];
    [UIView setAnimationRepeatCount:5];
    [UIView setAnimationDuration:0.07];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(earthquakeEnded:finished:context:)];
    
    view.transform = rightQuake; // end here & auto-reverse
    
    [UIView commitAnimations];
}

- (void)earthquakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    if ([finished boolValue]) {
        UIView* item = (__bridge UIView *)context;
        item.transform = CGAffineTransformIdentity;
    }
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((StaffMgrViewModel*)self.bindingContext).employees.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EPSStaffTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID forIndexPath:indexPath];
    
    cell.bindingContext = [((StaffMgrViewModel*)self.bindingContext).employees objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        id employee = [((StaffMgrViewModel*)self.bindingContext).employees objectAtIndex:indexPath.row];
        
        if([(StaffMgrViewModel*)self.bindingContext removeEmployee:employee])
        {
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }

    }
}

@end
