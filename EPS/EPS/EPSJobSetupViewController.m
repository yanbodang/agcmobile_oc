//
//  EPSJobSetupViewController.m
//  EPS
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSJobSetupViewController.h"
#import "Common.h"
#import "EPSJobSetupView.h"

#import <MessageUI/MFMailComposeViewController.h>

@interface EPSJobSetupViewController ()<MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic)EPSJobSetupView * jobSetupView;

-(void)addJobSetupView;

@end

@implementation EPSJobSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = TITLE_JOB_SETUP;
    
    [self addJobSetupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addJobSetupView
{
    self.jobSetupView = [[[NSBundle mainBundle] loadNibNamed:@"EPSJobSetupView" owner:self options:nil] objectAtIndex:0];
    [self.scrollView addSubview:self.jobSetupView];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, 500);
}

#pragma mark - MFMessageComposeViewControllerDelegate

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [[self parentViewController] dismissViewControllerAnimated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
