//
//  EPSFormTabBarController.m
//  EPS
//
//  Created by Yanbo Dang on 18/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSFormTabBarController.h"
#import "FormModel.h"
#import "JobModel.h"
#import "JobViewModel.h"
#import "FormViewModel.h"
#import "MBProgressHUD.h"
#import <AGCFramework/AGCFramework.h>

@interface EPSFormTabBarController ()<UITabBarControllerDelegate>

@property (strong, nonatomic)JobModel * job;

-(void)save:(id)sender;

-(void)back:(id)sender;

-(void)onBindingContextChanged:(FormModel*)model;

-(void)createRightBarItems;

-(void)createLeftBarItems;

@end

@implementation EPSFormTabBarController

@synthesize bindingContext = _bindingContext;

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    self.job = [JobViewModel getDefaultJob];
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.tabBar.tintColor = [AGCStyle darkOringe];
    [self createLeftBarItems];
    [self createRightBarItems];

    self.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)createRightBarItems
{
    UIBarButtonItem * btnSave = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconSaveWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(save:)];
    
    self.rightBarItems = @[btnSave];
}


-(void)createLeftBarItems
{
    UIBarButtonItem * btnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage agcResourceImageNamed:@"iconNaviBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    
    self.leftBarItems = @[btnBack];
}

-(void)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)save:(id)sender
{
    BOOL bRslt = YES;
    
    AGCBaseViewController * curSelController = (AGCBaseViewController *)self.selectedViewController;
    if([curSelController respondsToSelector:@selector(saveModel:)])
        bRslt = [curSelController saveModel:curSelController.bindingContext];
    
    if(bRslt)
    {
        MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Saved Successfully";
        [hud hide:YES afterDelay:1.0];
    }
}


-(void)setBindingContext:(id)bindingContext
{
    _bindingContext = bindingContext;
    
    [self onBindingContextChanged:((FormViewModel*)bindingContext).form];
}

-(void)onBindingContextChanged:(FormModel*)model
{
    model.JobName = model.JobName.length <= 0 ? self.job.Name : model.JobName;
    model.JobNumber = model.JobName.length <= 0 ? self.job.JobNumber : model.JobNumber;
    model.SiteForeman = model.SiteForeman.length <= 0 ? self.job.SiteForeman : model.SiteForeman;
    model.SiteLocation = model.SiteLocation.length <= 0 ? self.job.SiteLocation : model.SiteLocation;
    model.SiteForemanTitle = model.SiteForemanTitle.length <= 0 ? self.job.SiteForemanTitle : model.SiteForemanTitle;
}


-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    self.navigationItem.leftBarButtonItems = ((AGCBaseViewController*)viewController).leftBarItems;
    self.navigationItem.rightBarButtonItems = ((AGCBaseViewController*)viewController).rightBarItems;
}


@end
