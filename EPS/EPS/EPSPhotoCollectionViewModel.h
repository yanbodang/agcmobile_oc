//
//  EPSPhotoCollectionViewModel.h
//  EPS
//
//  Created by Yanbo Dang on 4/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>
#import <Photos/Photos.h>

@interface EPSPhotoCollectionViewModel : AGCPhotoCollectionViewModel

@property(nonatomic, strong)PHFetchResult * assets;

-(BOOL)hasSelectedAsset:(NSUInteger)index;

-(void)selectAssetAtIndex:(NSUInteger)index;

-(void)deselectAssetAtIndex:(NSUInteger)index;

@end
