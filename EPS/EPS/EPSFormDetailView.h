//
//  EPSFormDetailView.h
//  EPS
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>

@protocol EPSFormDetailViewProtocol <NSObject>

-(void)requestDisableScroll:(BOOL)bDisable;

@end

@interface EPSFormDetailView : AGCBaseView

@property(nonatomic, assign)id<EPSFormDetailViewProtocol> delegate;

@end
