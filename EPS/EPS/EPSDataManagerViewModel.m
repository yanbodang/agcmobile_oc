//
//  EPSDataManagerViewModel.m
//  EPS
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSDataManagerViewModel.h"

@implementation EPSDataManagerViewModel

-(instancetype)init
{
    self = [super init];
    return self;
}

-(void)downloadDataWithCompleted:(void (^)(BOOL success))handle
{
    __block BOOL bRslt = YES;
    
    AGCHttpClient * httpClient = self.biz.httpClient;
    

    dispatch_group_t task = dispatch_group_create();
    
    dispatch_group_enter(task);
    
    [httpClient getURL:API_GETALLJOBS parameter:nil success:^(id respond) {
        
        NSDictionary * dic = (NSDictionary*)respond;
        
        for (NSString* key in dic)
        {
            NSArray * values  = [dic objectForKey:key];
            [DBServiceProvider insertOrReplaceOnTable:@"Job" withKeyValuePairs:values onDB:[EPSBizLogic shareInstance].sDBName];
        }
        
        dispatch_group_leave(task);
        
    } failure:^(NSError *error) {
        
        bRslt = bRslt & NO;
        dispatch_group_leave(task);
    }];
    
    dispatch_group_enter(task);
    
    [httpClient getURL:API_GETALLPREFERENCES parameter:nil success:^(id respond) {
        
        NSDictionary * dic = (NSDictionary*)respond;
        
        for (NSString* key in dic)
        {
            NSArray * values  = [dic objectForKey:key];
            [DBServiceProvider insertOrReplaceOnTable:@"Preference" withKeyValuePairs:values onDB:[EPSBizLogic shareInstance].sDBName];
        }
        
        dispatch_group_leave(task);
        
    } failure:^(NSError *error) {
        bRslt = bRslt & NO;
        dispatch_group_leave(task);
    }];
    

    
    dispatch_group_notify(task, dispatch_get_main_queue(), ^{
        if(bRslt)
            BLOCK_SAFE_RUN(handle, YES)
        else
            BLOCK_SAFE_RUN(handle, NO)
            });
}

@end
