//
//  AttendanceViewModel.h
//  EPS
//
//  Created by Yanbo Dang on 1/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSBaseViewModel.h"

@class FormModel;

@interface AttendanceViewModel : EPSBaseViewModel

@property(nonatomic, strong)NSArray * attendances;

-(void)setAllAttendanceDateTime:(NSString*)dateTime;

@end
