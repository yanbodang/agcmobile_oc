//
//  EPSFormDetailViewController.m
//  EPS
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSFormDetailViewController.h"
#import "EPSFormDetailView.h"
#import "EPSFormTabBarController.h"

#define CONTENT_SIZE_HEIGHT 1000.0

@interface EPSFormDetailViewController ()<EPSFormDetailViewProtocol>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic)EPSFormDetailView * formDetailView;

-(void)setupFormDetailsView;

@end

@implementation EPSFormDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.bindingContext = ((EPSFormTabBarController*)self.tabBarController).bindingContext;
    [self setupFormDetailsView];
    
    self.leftBarItems = ((EPSFormTabBarController*)self.tabBarController).leftBarItems;
    self.rightBarItems = ((EPSFormTabBarController*)self.tabBarController).rightBarItems;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupFormDetailsView
{
    self.formDetailView = [[[NSBundle mainBundle] loadNibNamed:@"EPSFormDetailView" owner:self options:nil] objectAtIndex:0];
    self.formDetailView.delegate = self;
    [self.scrollView addSubview:self.formDetailView];
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, CONTENT_SIZE_HEIGHT);
    self.formDetailView.bindingContext = self.bindingContext;
}

-(BOOL)saveModel:(id)model
{
    BOOL bRslt = YES;
    
    if([self.formDetailView respondsToSelector:@selector(save)])
    {
        bRslt = [self.formDetailView save];
    }
    
    return bRslt;
}

#pragma mark - EPSFormDetailViewDelegate

-(void)requestDisableScroll:(BOOL)bDisable
{
    self.scrollView.scrollEnabled = !bDisable;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
