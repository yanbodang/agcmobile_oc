//
//  EPSPhotoDetailViewModel.m
//  EPS
//
//  Created by Yanbo Dang on 4/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSPhotoDetailViewModel.h"
#import "FormPhotoModel.h"
#import "MJExtension.h"

@interface EPSPhotoDetailViewModel()

@property(nonatomic,weak)AGCPhotoModel * photoModel;

@end

@implementation EPSPhotoDetailViewModel

-(void)initViewModelWithParam:(id)param completed:(void (^)(id))completedHandle
{
    self.photoModel = (AGCPhotoModel*)param;
}

-(UIImage*)getImage
{
    return self.photoModel.image;
}

-(NSString*)comment
{
    return self.photoModel.comment;
}

-(void)setComment:(NSString *)comment
{
    self.photoModel.comment = comment;
}

-(BOOL)save
{
    
    FormPhotoModel * model = (FormPhotoModel *)self.photoModel.rawModel;
    model.Description = self.comment;
    
    return [DBServiceProvider insertOrReplaceOnTable:@"FormPhoto" withKeyValuePair:[model mj_keyValues] onDB:self.biz.sDBName];
}

@end
