//
//  FormChecklistModel.m
//  EPS
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "FormChecklistModel.h"
#import "JobModel.h"
#import "PreferenceModel.h"


@implementation FormChecklistModel

-(instancetype)initWithJob:(JobModel*)job Preference:(PreferenceModel*)pref date:(NSString*)date
{
    self = [super init];
    
    self.JobNumber=job.JobNumber;
    self.SiteForemanEmail = job.SiteForemanEmail;
    self.Date = date;
    self.PreferenceName= pref.PreferenceName;
    self.PreferenceCode = pref.Code;
    
    if(pref.Code.length == 0)
        self.SortOrder = [NSString stringWithFormat:@"1000000%@", pref.Code];
    
    if(pref.Code.length == 1)
        self.SortOrder = [NSString stringWithFormat:@"100000%@", pref.Code];
    
    if(pref.Code.length == 2)
        self.SortOrder = [NSString stringWithFormat:@"10000%@", pref.Code];
    
    if(pref.Code.length == 3)
        self.SortOrder = [NSString stringWithFormat:@"1000%@", pref.Code];
    
    if(pref.Code.length == 4)
        self.SortOrder = [NSString stringWithFormat:@"100%@", pref.Code];
    
    self.Item=pref.Value;
    
    return self;
}

@end
