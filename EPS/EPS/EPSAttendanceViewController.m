//
//  EPSAttendanceViewController.m
//  EPS
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSAttendanceViewController.h"
#import "AttendanceViewModel.h"
#import "EPSFormTabBarController.h"
#import "EPSAttendanceTableViewCell.h"
#import "EPSDateTimeView.h"
#import "FormViewModel.h"

static NSString * CELL_ID = @"EPSAttendanceTableViewCell";

@interface EPSAttendanceViewController ()<EPSAttendanceTableViewCellDelegate>

-(void)changeAllAttendanceTime:(NSString*)dateTime;

@end

@implementation EPSAttendanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.estimatedRowHeight = 95;
    [self.tableView registerNib:[UINib nibWithNibName:@"EPSAttendanceTableViewCell" bundle:nil] forCellReuseIdentifier:CELL_ID];
    
    AttendanceViewModel * vm = [[AttendanceViewModel alloc] init];
    [vm initViewModelWithParam:((FormViewModel*)((EPSFormTabBarController*)self.tabBarController).bindingContext).form completed:nil];
    
    self.bindingContext = vm;
    
    self.leftBarItems = ((EPSFormTabBarController*)self.tabBarController).leftBarItems;
    self.rightBarItems = ((EPSFormTabBarController*)self.tabBarController).rightBarItems;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSString*)headerTitleForSecion:(NSInteger)secion
{
    return @"Manage your staff's attendance";
}

-(void)requestChangeTime:(id)sender
{
    
    EPSAttendanceTableViewCell * cell = (EPSAttendanceTableViewCell*)sender;
    
    EPSDateTimeView * dateTimeView = [[[NSBundle mainBundle] loadNibNamed:@"EPSDateTimeView" owner:self options:nil] objectAtIndex:0];
    dateTimeView.translatesAutoresizingMaskIntoConstraints = NO;
    dateTimeView.dateTime = cell.dateTime;
    
    
    KLCPopup * popup = [KLCPopup popupWithContentView:dateTimeView showType:KLCPopupShowTypeFadeIn dismissType:KLCPopupDismissTypeFadeOut maskType:KLCPopupMaskTypeDimmed dismissOnBackgroundTouch:NO dismissOnContentTouch:NO];
    
    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter, KLCPopupVerticalLayoutCenter);
    
    __weak typeof(self) weakSelf = self;
    dateTimeView.confirmHandel = ^(NSString *dateTime, BOOL bApplyAll) {
        
        if(bApplyAll)
        {
            [((AttendanceViewModel*)weakSelf.bindingContext) setAllAttendanceDateTime:dateTime];
            [weakSelf.tableView reloadData];
        }
        else
        {
            cell.dateTime = dateTime;
        }
        
        [popup dismiss:YES];
        
    };
    
    dateTimeView.cancelHandel = ^{
        
        [popup dismiss:YES];
        
    };
    
    
    
    [popup showWithLayout:layout];
}

-(void)changeAllAttendanceTime:(NSString*)dateTime
{
    
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((AttendanceViewModel*)self.bindingContext).attendances.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EPSAttendanceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID forIndexPath:indexPath];
    
    cell.bindingContext = [((AttendanceViewModel*)self.bindingContext).attendances objectAtIndex:indexPath.row];
    cell.delegate = self;
    
    return cell;
}

@end
