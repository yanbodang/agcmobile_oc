//
//  FormAttendanceModel.h
//  EPS
//
//  Created by Yanbo Dang on 31/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>

@class JobModel;
@class EmployeeModel;
@class FormModel;

@interface FormAttendanceModel : BaseModel

@property (nonatomic, copy) NSString *JobNumber;
@property (nonatomic, copy) NSString *SiteForemanEmail;
@property (nonatomic, copy) NSString *Date;
@property (nonatomic, copy) NSString *EmployeeName;
@property (nonatomic, copy) NSString *Attendance;
@property (nonatomic, copy) NSString *Comment;
@property (nonatomic, copy) NSString *Company;


-(instancetype)initWithJob:(JobModel*)job employee:(EmployeeModel*)employee form:(FormModel*)form;

@end
