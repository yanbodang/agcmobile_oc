//
//  EmployeeModel.h
//  EPS
//
//  Created by Yanbo Dang on 30/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>

@class JobModel;

@interface EmployeeModel : BaseModel

@property (nonatomic, copy) NSString * Name;
@property (nonatomic, copy) NSString * SiteForemanEmail;
@property (nonatomic, copy) NSString * JobNumber;
@property (nonatomic, copy) NSString * Company;
@property (nonatomic, assign) NSInteger ActiveFlag;

-(instancetype)initWithName:(NSString*)name company:(NSString*)company job:(JobModel*)job;

@end
