//
//  EPSDataManagerViewModel.h
//  EPS
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSBaseViewModel.h"

@interface EPSDataManagerViewModel : EPSBaseViewModel

-(void)downloadDataWithCompleted:(void (^)(BOOL success))handle;

@end
