//
//  JobViewModel.h
//  EPS
//
//  Created by Yanbo Dang on 23/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSBaseViewModel.h"

@class JobModel;

@interface JobViewModel : EPSBaseViewModel

+(void)getAllJobsWithCompleted:(void (^)(NSError * error))completed;

+(JobModel*)getJobByJobNumber:(NSString *)jobNumber;

+(BOOL)validJobNumber:(NSString*)jobNumber;

+(JobModel*)getDefaultJob;

@end
