//
//  EPSJobSetupView.m
//  EPS
//
//  Created by Yanbo Dang on 25/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSJobSetupView.h"
#import <AGCFramework/AGCFramework.h>
#import "Common.h"
#import "JobViewModel.h"
#import "MBProgressHUD.h"

@interface EPSJobSetupView()

@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *txtJobNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;


- (IBAction)didSave:(id)sender;
- (IBAction)didLogout:(id)sender;
- (IBAction)sendDB:(id)sender;

-(BOOL)verify;


@end

@implementation EPSJobSetupView

@synthesize bindingContext = _bindingContext;


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.txtEmail.text = [AGCUtility getUserDefaultValueForKey:KEY_SITE_FOREMAN_EMAIL];
    self.txtJobNumber.text = [AGCUtility getUserDefaultValueForKey:KEY_JOB_NUMBER];
    
    if([self verify])
    {
        self.lblMessage.text = [NSString stringWithFormat:@"You have full access to use the form with job number %@", self.txtJobNumber.text];
    }
}

- (IBAction)didSave:(id)sender {
    
    
    if(![self verify])
    {
        self.lblMessage.text = @"Currently, you don't have access to use the form. Please contact  your project manager";
        
        [AGCUtility showAlert:@"Job / Site foreman Not Valid"
                   message:@"Please enter a valid job number and email address. Please download the data (from the menu) if you haven't done so"
              onController:[self parentViewController] defaultAction:nil defaultActionTitle:SOK];
        return;
    }
    
    [AGCUtility saveUserDefaultValue:self.txtJobNumber.text forKey:KEY_JOB_NUMBER];
    [AGCUtility saveUserDefaultValue:self.txtEmail.text forKey:KEY_SITE_FOREMAN_EMAIL];
    
    self.lblMessage.text = [NSString stringWithFormat:@"You have full access to use the form with job number %@", self.txtJobNumber.text];;
    
    [AGCUtility showAlert:SUCCESS message:@"The job is valid" onController:[self parentViewController] defaultAction:nil defaultActionTitle:SOK];
    
    
}

- (IBAction)didLogout:(id)sender {
    
    [AGCUtility saveUserDefaultValue:SEmpty forKey:KEY_JOB_NUMBER];
    [AGCUtility saveUserDefaultValue:SEmpty forKey:KEY_SITE_FOREMAN_EMAIL];
    
    
    self.txtJobNumber.text = SEmpty;
    self.txtEmail.text = SEmpty;
    
    self.lblMessage.text = @"Currently, you don't have access to use the form. Please contact  your project manager";
    //TODO: report issue;
}

- (IBAction)sendDB:(id)sender {
    
    UIViewController * parentViewController = [self parentViewController];
    
    AGCEmailModel * email = [[AGCEmailModel alloc]  init];
    email.subject = @"Send Error";
    email.message = @"Database attached";
    email.recipients = @[@"ddiep@agcoombs.com.au",@"ydang@agcoombs.com.au"];
    email.attachmentMimeType = @"application/x-sqlite3";
    email.attachmentPath = [EPSBizLogic shareInstance].sDBName;
    
    [AGCUtility sendEmail:email onViewController:parentViewController];
}


-(BOOL)verify
{
    BOOL bRslt = YES;
    
    //Eamil
    bRslt = bRslt & [AGCUtility verifyEmail:self.txtEmail.text];
    //Job Number
    bRslt = bRslt & [JobViewModel validJobNumber:self.txtJobNumber.text];
    
    return bRslt;
}


@end
