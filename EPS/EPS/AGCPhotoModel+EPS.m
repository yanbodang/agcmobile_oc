//
//  AGCPhotoModel+EPS.m
//  EPS
//
//  Created by Yanbo Dang on 4/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AGCPhotoModel+EPS.h"
#import "FormPhotoModel.h"

@implementation AGCPhotoModel (EPS)

-(instancetype)initWithFormPhotoModel:(FormPhotoModel*)model
{
    self = [super init];
    self.image = [UIImage imageWithData:model.ImageData];
    self.comment = model.Description;
    self.rawModel = model;
    return self;
}

@end
