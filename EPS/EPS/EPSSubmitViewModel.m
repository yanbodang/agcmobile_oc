//
//  EPSSubmitViewModel.m
//  EPS
//
//  Created by Yanbo Dang on 5/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSSubmitViewModel.h"
#import <AGCFramework/AGCFramework.h>
#import "Common.h"
#import "FormModel.h"
#import "JobModel.h"
#import "JobViewModel.h"
#import "MJExtension.h"
#import "FormPhotoModel.h"
#import "FormChecklistModel.h"

@interface EPSSubmitViewModel()

@property(nonatomic,weak)FormModel * form;
@property(nonatomic,strong)JobModel * job;
@property(nonatomic, strong)NSArray * formAnsweredQuestions;

-(NSArray *)getAttachedPhotos;

-(NSArray *)getAnsweredFormQuestions;

-(NSString *)getAttendanceJson;

-(NSError *)verifyForm;

@end

@implementation EPSSubmitViewModel

-(instancetype)init
{
    self = [super init];
    self.job = [JobViewModel getDefaultJob];
    
    return self;
}

-(void)initViewModelWithParam:(id)param completed:(void (^)(id))completedHandle
{
    self.form = (FormModel*)param;
    
    NSArray * arrEmail = [AGCUtility getUserDefaultValueForKey:KEY_DIST_EMAIL_LST];
    NSMutableArray * emails = [[NSMutableArray alloc] init];
    if(nil != arrEmail) [emails addObjectsFromArray:arrEmail];
    
    self.emails = emails;
}

-(void)appendEmail:(NSString*)email
{
    NSMutableArray * emails = (NSMutableArray*)self.emails;
    [emails addObject:email];
    self.emails = emails;
    [AGCUtility saveUserDefaultValue:self.emails forKey:KEY_DIST_EMAIL_LST];
}

-(void)removeEmail:(NSString*)email
{
    NSMutableArray * emails = (NSMutableArray*)self.emails;
    [emails removeObject:email];
    self.emails = emails;
    [AGCUtility saveUserDefaultValue:self.emails forKey:KEY_DIST_EMAIL_LST];
}

-(NSString*)toEmailsRecipients
{
    return [self.emails componentsJoinedByString:@";"];
}

-(NSArray*)getAttachedPhotos
{
    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM FormPhoto WHERE SiteForemanEmail = '%@' AND Date = '%@' ORDER BY SortOrder", self.job.SiteForemanEmail, self.form.Date];
    
    __block NSMutableArray * photos = [[NSMutableArray alloc] init];
    
    [DBServiceProvider querySql:sSql onDB:self.biz.sDBName queryResult:^(id result, BOOL finished) {
        if(!finished)
        {
            [photos addObject:[FormPhotoModel mj_objectWithKeyValues:result]];
        }
    }];
    
    return photos;
}

-(NSArray *)getAnsweredFormQuestions
{

    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM FormCheckList WHERE JobNumber = '%@' AND SiteForemanEmail = '%@' AND Date = '%@' AND Respond Is Not Null", self.job.JobNumber, self.job.SiteForemanEmail, self.form.Date];
    
    __block NSMutableArray * questions = [[NSMutableArray alloc] init];
    
    [DBServiceProvider querySql:sSql onDB:self.biz.sDBName queryResult:^(id result, BOOL finished) {
        if(!finished)
        {
            [questions addObject:result];
        }
    }];
    
    return questions;
    /*
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:questions options:NSJSONWritingPrettyPrinted  error:nil];
    NSString * sJson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"%@",sJson);
    return sJson;
     */
}

-(NSString *)getAttendanceJson
{
    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM FormAttendance WHERE JobNumber = '%@' AND SiteForemanEmail = '%@' AND Date = '%@' ", self.job.JobNumber, self.job.SiteForemanEmail, self.form.Date];

    __block NSMutableArray * attendances = [[NSMutableArray alloc] init];
    
    [DBServiceProvider querySql:sSql onDB:self.biz.sDBName queryResult:^(id result, BOOL finished) {
        if(!finished)
        {
            [attendances addObject:result];
        }
    }];
    
    return [attendances mj_JSONString];
    
    /*
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:attendances options:NSJSONWritingPrettyPrinted  error:nil];
    NSString * sJson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"%@",sJson);
    return sJson;
     */
}

-(NSError *)verifyForm
{
    NSError * error = nil;
    
    NSString * sErr = nil;
    //answer all the questions
    self.formAnsweredQuestions = [self getAnsweredFormQuestions];
    if(self.formAnsweredQuestions.count != 17)
    {
        sErr = @"Please enter all the questions on the check list tab";
    }
    //
    if([self.form.SafetyConcernFlag isEqualToString:SYES] && self.form.SafetyConcernDescription.length <= 0)
    {
        if(nil != sErr)
            sErr = [sErr stringByAppendingString:@" and provide the details for your work safety concerns question"];
        else
            sErr = @"You must provide the details for your work safety concerns question";
    }
    
    if(nil != sErr)
    {
        error = [NSError errorWithDomain:@"EPS" code:01 userInfo:@{@"ErrMsg" : sErr}];
    }
    return error;
}

-(BOOL)saveModel:(id)model parameter:(id)param completedHandler:(void (^)(NSError * _Nullable))completed
{
    
    NSError * error = [self verifyForm];
    
    if(nil != error)
    {
        BLOCK_SAFE_RUN(completed, error)
        return NO;
    }
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    
    NSString *sessionID = [AGCUtility getUUID];
    

    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *sessionDate = [formatter stringFromDate:[NSDate date]];
    
    NSMutableDictionary * formDic = [[NSMutableDictionary alloc] initWithDictionary:[self.form mj_keyValuesWithIgnoredKeys:@[@"Signature"]]];
    [formDic setValue:sessionID forKey:KEY_SESSIONID];
    [formDic setValue:version forKey:KEY_APPVER];
    [formDic setValue:sessionDate forKey:KEY_SESSION_DATE];
    [formDic setValue:[self toEmailsRecipients] forKey:KEY_EMAIL_LIST];
    
    //NSData * jsonData = [NSJSONSerialization dataWithJSONObject:formDic options:NSJSONWritingPrettyPrinted  error:nil];
    //NSString * sJson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",sJson);

    
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    [payload setObject:ESP forKey:KEY_FORM_TYPE];
    [payload setObject:[formDic mj_JSONString] forKey:KEY_FORM];
    [payload setObject:[self.formAnsweredQuestions mj_JSONString] forKey:KEY_QA_ARR];
    [payload setObject:[self getAttendanceJson] forKey:KEY_ATTENDNC_ARR];
    
    NSArray * photos = [self getAttachedPhotos];
    NSArray * photoKeyValuePairs = [FormPhotoModel mj_keyValuesArrayWithObjectArray:photos ignoredKeys:@[@"ImageData"]];

    //jsonData = [NSJSONSerialization dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted  error:nil];
    //sJson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",sJson);
    
    [payload setObject:[photoKeyValuePairs mj_JSONString] forKey:KEY_PHOTO_ARR];
    
    [self.biz.httpClient uploadToUrl:API_POSTDATA_V2 parameter:payload progress:^(double completedPercent) {
        
        BLOCK_SAFE_RUN(self.submitProgres, completedPercent)
        
    } constructData:^NSArray<__kindof AGCHttpFormFile *> *{
        
        NSMutableArray * uploadFiles = [[NSMutableArray alloc] init];
        
        AGCHttpFormFile * file = [[AGCHttpFormFile alloc] init];
        file.name = [NSString stringWithFormat:@"%@%@",self.form.SiteForemanEmail,self.form.Date];
        file.extension = @".jpg";
        file.fileType = kJpe;
        file.data = self.form.Signature;
        [uploadFiles addObject:file];
        
        for(FormPhotoModel * photo in photos)
        {
            AGCHttpFormFile * file = [[AGCHttpFormFile alloc] init];
            file.name = photo.PhotoID;
            file.extension = @".jpg";
            file.data = photo.ImageData;
            file.fileType = kJpe;
            
            [uploadFiles addObject:file];
        }
        return uploadFiles;
        
    } success:^(id response) {
    
        self.form.CompletedFlag = SYES;
        [DBServiceProvider insertOrReplaceOnTable:@"Form" withKeyValuePair:[self.form mj_keyValues] onDB:self.biz.sDBName];
        BLOCK_SAFE_RUN(completed,nil)
        NSLog(@"submit successfully");
        
    } failure:^(NSError *error) {
        
        BLOCK_SAFE_RUN(completed, error)
        NSLog(@"submit failed");
        
    }];
    
    return YES;
}

@end
