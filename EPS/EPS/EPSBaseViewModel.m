//
//  EPSBaseViewModel.m
//  EPS
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSBaseViewModel.h"


@implementation EPSBaseViewModel

-(instancetype)init
{
    self = [super init];
    self.biz = [EPSBizLogic shareInstance];
    
    return self;
}

@end
