//
//  EPSPhotoCollectionViewModel.m
//  EPS
//
//  Created by Yanbo Dang on 4/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSPhotoCollectionViewModel.h"
#import "FormModel.h"
#import "JobModel.h"
#import "JobViewModel.h"
#import "FormPhotoModel.h"
#import "MJExtension.h"
#import "AGCPhotoModel+EPS.h"

@interface EPSPhotoCollectionViewModel()
{
    dispatch_queue_t loadphotoqueue;
}

@property(nonatomic, strong)JobModel * job;
@property(nonatomic, weak)FormModel * form;
@property(nonatomic, strong)NSMutableArray * selAssetIdx;

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;


@end

@implementation EPSPhotoCollectionViewModel

@synthesize photos = _photos;

-(instancetype)init
{
    self = [super init];
    
    loadphotoqueue = dispatch_queue_create("LoadingPhotoQueue", DISPATCH_QUEUE_SERIAL);
    
    return self;
}

-(void)initViewModelWithParam:(id)param completed:(void (^)(id))completedHandle
{
    self.job = [JobViewModel getDefaultJob];
    self.form = (FormModel*)param;
    
    self.selAssetIdx = [[NSMutableArray alloc] init];
    
    __weak typeof(self) weakSelf = self;
    
    dispatch_async(loadphotoqueue, ^{
        
        __block NSMutableArray * photos = [[NSMutableArray alloc] init];
        NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM FormPhoto WHERE SiteForemanEmail = '%@' AND Date = '%@' ORDER BY SortOrder", self.job.SiteForemanEmail, self.form.Date];
        [DBServiceProvider querySql:sSql onDB:[EPSBizLogic shareInstance].sDBName queryResult:^(id result, BOOL finished) {
            if(!finished)
            {
                AGCPhotoModel * model = [[AGCPhotoModel alloc] initWithFormPhotoModel:[FormPhotoModel mj_objectWithKeyValues:result]];
                [photos addObject:model];
            }
        }];
        
        //NSSortDescriptor *sort=[NSSortDescriptor sortDescriptorWithKey:@"SortOrder" ascending:YES];
        //[photos sortUsingDescriptors:[NSArray arrayWithObject:sort]];
        
        weakSelf.photos = photos;
        
        BLOCK_SAFE_RUN(completedHandle,weakSelf)

    });

}

-(NSArray*)photos
{
    if(self.selAssetIdx.count > 0)
    {
        __block NSMutableArray * images = [[NSMutableArray alloc] init];
        
        PHImageRequestOptions * imgReqOpt = [[PHImageRequestOptions alloc] init];
        imgReqOpt.resizeMode = PHImageRequestOptionsResizeModeExact;
        imgReqOpt.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
        imgReqOpt.synchronous = YES;
        
        for(NSNumber * number in self.selAssetIdx)
        {
            PHAsset * asset = [self.assets objectAtIndex:[number integerValue]];
            [[PHImageManager defaultManager] requestImageForAsset:asset targetSize:CGSizeMake(800.0, 800.0) contentMode:PHImageContentModeAspectFill options:imgReqOpt resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                if(nil != result)
                    [images addObject:result];
                
            }];
        }
        [self.selAssetIdx removeAllObjects]; //make sure to clear this array for avoiding infinite loop
        
        for(UIImage * image in images)
        {
            [self appendPhoto:image comment:@""];
        }
        [images removeAllObjects];
    }
    
    return _photos;
}

-(BOOL)appendPhoto:(UIImage*)image comment:(NSString*)comment
{
    BOOL bRslt = NO;
    
    UIImage * img = [self imageWithImage:image scaledToSize:CGSizeMake(800.0, 800.0)];
    
    FormPhotoModel * formPhotoModel = [[FormPhotoModel alloc] init];
    formPhotoModel.SiteForemanEmail = self.form.SiteForemanEmail;
    formPhotoModel.PhotoID = [AGCUtility getUniqueGUID];
    formPhotoModel.Date = self.form.Date;
    formPhotoModel.Description = comment;
    formPhotoModel.SortOrder = [NSString stringWithFormat:@"%lu",(unsigned long)_photos.count];
    formPhotoModel.ImageData = UIImageJPEGRepresentation(img, 1.0);

    AGCPhotoModel * photoModel = [[AGCPhotoModel alloc] initWithImage:img comment:comment];
    photoModel.rawModel = formPhotoModel;
    
    if( (bRslt = [DBServiceProvider insertOrReplaceOnTable:@"FormPhoto" withKeyValuePair:[formPhotoModel mj_keyValues] onDB:[EPSBizLogic shareInstance].sDBName]))
    {
        NSMutableArray * photos = (NSMutableArray*)self.photos;
        
        [photos addObject:photoModel];
        
        self.photos = photos;
    }
    
    return bRslt;
}

-(BOOL)removePhoto:(AGCPhotoModel *)photoModel
{
    BOOL bRslt = NO;
    
    FormPhotoModel * formPhotoModel = (FormPhotoModel*)photoModel.rawModel;
    NSString * sSql = [NSString stringWithFormat:@"DELETE FROM FormPhoto WHERE PhotoID = '%@'", formPhotoModel.PhotoID];
    if((bRslt = [DBServiceProvider executeSql:sSql onDB:[EPSBizLogic shareInstance].sDBName]))
    {
        NSMutableArray * photos = (NSMutableArray*)self.photos;
        [photos removeObject:photoModel];
        self.photos = photos;
    }

    return bRslt;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


-(BOOL)hasSelectedAsset:(NSUInteger)index
{
    BOOL bFound = NO;
    
    for(NSNumber * number in self.selAssetIdx)
    {
        if([number unsignedIntegerValue] == index)
        {
            bFound = YES;
            break;
        }
    }
    return bFound;
}

-(void)selectAssetAtIndex:(NSUInteger)index
{
    [self.selAssetIdx addObject:[NSNumber numberWithUnsignedInteger:index]];
}

-(void)deselectAssetAtIndex:(NSUInteger)index
{
    for(NSNumber * number in self.selAssetIdx)
    {
        if([number unsignedIntegerValue] == index)
        {
            [self.selAssetIdx removeObject:number];
            break;
        }
    }
}

@end
