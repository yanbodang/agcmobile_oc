//
//  AttendanceViewModel.m
//  EPS
//
//  Created by Yanbo Dang on 1/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "AttendanceViewModel.h"
#import "JobModel.h"
#import "JobViewModel.h"
#import "EmployeeModel.h"
#import "FormAttendanceModel.h"
#import "MJExtension.h"
#import "FormModel.h"

@interface AttendanceViewModel()

@property(nonatomic, strong)JobModel * defaultJob;
@property(nonatomic, weak)FormModel * form;

+(NSArray*)getEmployeesByQuery:(NSString*)query;

+(NSArray*)getEmployeesByJobNumber:(NSString*)jobNumber siteForemanEmail:(NSString*)email date:(NSString*)date;


+(FormAttendanceModel*)getFormAttendanceByQuery:(NSString*)query;

@end

@implementation AttendanceViewModel


-(void)initViewModelWithParam:(id)param completed:(void (^)(id))completedHandle
{
    self.defaultJob = [JobViewModel getDefaultJob];
    self.form = (FormModel*)param;
    
    
    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM Employee WHERE SiteForemanEmail = '%@' AND JobNumber = '%@' ORDER BY Name ASC", self.defaultJob.SiteForemanEmail, self.defaultJob.JobNumber];
    
    NSArray * employees = [AttendanceViewModel getEmployeesByQuery:sSql];
    
    for(EmployeeModel * employee in employees)
    {
        NSString * sSqlAttendance = [NSString stringWithFormat:@"SELECT * FROM FormAttendance  WHERE JobNumber = '%@' AND SiteForemanEmail = '%@' AND Date = '%@' AND EmployeeName = '%@'", self.defaultJob.JobNumber, self.defaultJob.SiteForemanEmail, self.form.Date, employee.Name];
        
        FormAttendanceModel * attendance = [AttendanceViewModel getFormAttendanceByQuery:sSqlAttendance];
        if(nil == attendance)
        {
            attendance = [[FormAttendanceModel alloc] initWithJob:self.defaultJob employee:employee form:self.form];
            [DBServiceProvider insertOrReplaceOnTable:@"FormAttendance" withKeyValuePair:[attendance mj_keyValues] onDB:self.biz.sDBName];
        }
    }
}

-(NSArray*)attendances
{
    if(nil != _attendances)
        return _attendances;
    
    _attendances = [AttendanceViewModel getEmployeesByJobNumber:self.defaultJob.JobNumber siteForemanEmail:self.defaultJob.SiteForemanEmail date:self.form.Date];

    return _attendances;
}

-(void)setAllAttendanceDateTime:(NSString*)dateTime
{
    for(FormAttendanceModel * model in self.attendances)
        model.Comment = dateTime;
}

-(BOOL)save
{
    return [DBServiceProvider insertOrReplaceOnTable:@"FormAttendance" withKeyValuePairs:[FormAttendanceModel mj_keyValuesArrayWithObjectArray:self.attendances] onDB:self.biz.sDBName];
}


+(NSArray*)getEmployeesByJobNumber:(NSString*)jobNumber siteForemanEmail:(NSString*)email date:(NSString*)date
{
    __block NSMutableArray * attendances = [[NSMutableArray alloc] init];
    
    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM FormAttendance WHERE JobNumber = '%@' AND SiteForemanEmail = '%@' AND Date = '%@' ", jobNumber, email, date];
    
    [DBServiceProvider querySql:sSql onDB:[EPSBizLogic shareInstance].sDBName queryResult:^(id result, BOOL finished) {
        if(!finished)
            [attendances addObject:[FormAttendanceModel mj_objectWithKeyValues:result]];
        }];
    
    NSSortDescriptor *sort=[NSSortDescriptor sortDescriptorWithKey:@"EmployeeName" ascending:YES];
    [attendances sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    
    return attendances;
}

+(FormAttendanceModel*)getFormAttendanceByQuery:(NSString*)query
{
    __block FormAttendanceModel * model = nil;
    
    [DBServiceProvider querySql:query onDB:[EPSBizLogic shareInstance].sDBName queryResult:^(id result, BOOL finish)
     {
         if(!finish)
             model = [FormAttendanceModel mj_objectWithKeyValues:result];
     }];
    return model;
}

+(NSArray*)getEmployeesByQuery:(NSString*)query
{
    __block NSMutableArray * records = [[NSMutableArray alloc] init];
    
    [DBServiceProvider querySql:query onDB:[EPSBizLogic shareInstance].sDBName queryResult:^(id result, BOOL finish)
     {
         if(!finish)
             [records addObject:[EmployeeModel mj_objectWithKeyValues:result]];
     }];
    
    return records;
}

@end
