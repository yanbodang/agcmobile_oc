//
//  EPSBaseViewModel.h
//  EPS
//
//  Created by Yanbo Dang on 24/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>
#import "EPSBizLogic.h"

@interface EPSBaseViewModel : BaseViewModel

@property(nonatomic, weak)EPSBizLogic * biz;

@end
