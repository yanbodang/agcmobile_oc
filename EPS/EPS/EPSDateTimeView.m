//
//  EPSDateTimeView.m
//  EPS
//
//  Created by Yanbo Dang on 1/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSDateTimeView.h"

@interface EPSDateTimeView()

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

- (IBAction)clicked:(id)sender;
- (IBAction)confirm:(id)sender;
- (IBAction)cancel:(id)sender;


@end

@implementation EPSDateTimeView

@synthesize shouldApplyAll = _shouldApplyAll;
@synthesize dateTime = _dateTime;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)setDateTime:(NSString *)dateTime
{
    _dateTime = dateTime;
    
    NSDate * date = [AGCUtility dateFromString:dateTime formats:[AGCUtility getDataFormates]];
    if(nil != date)
        self.datePicker.date = date;
    else
        self.datePicker.date = [AGCUtility dataFromString:@"06:45 AM" format:@"HH:mm a"];
}

- (IBAction)clicked:(id)sender {
    
    UIButton * btn = (UIButton*)sender;
    btn.selected = !btn.selected;
    _shouldApplyAll = btn.selected;
}

- (IBAction)confirm:(id)sender {
    
    BLOCK_SAFE_RUN(self.confirmHandel, self.dateTime, self.shouldApplyAll)
}

- (IBAction)cancel:(id)sender {
    
    BLOCK_SAFE_RUN(self.cancelHandel)
}

-(NSString*)dateTime
{
    return _dateTime = [AGCUtility date:self.datePicker.date format:@"hh:mm a"];
}

@end
