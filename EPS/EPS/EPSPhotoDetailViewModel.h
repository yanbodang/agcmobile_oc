//
//  EPSPhotoDetailViewModel.h
//  EPS
//
//  Created by Yanbo Dang on 4/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSBaseViewModel.h"

@interface EPSPhotoDetailViewModel : EPSBaseViewModel

@property(nonatomic,readonly,getter=getImage)UIImage * image;
@property(nonatomic,copy)NSString * comment;

@end
