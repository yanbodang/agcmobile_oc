//
//  EPSHomeTableViewController.m
//  EPS
//
//  Created by Yanbo Dang on 21/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSHomeTableViewController.h"
#import "Common.h"
#import "FormsViewModel.h"
#import "EPSFormTableViewCell.h"
#import "EPSFormTabBarController.h"
#import "FormViewModel.h"

#define CELL_ID @"EPSFormTableViewCell"

@interface EPSHomeTableViewController ()

@property(nonatomic, weak,getter=getSelFormModel)FormModel * form;

@end

@implementation EPSHomeTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = TITLE_PRESTART_FORM;
    
    FormsViewModel * viewModel = [[FormsViewModel alloc] init];
    
    [viewModel initViewModelWithParam:nil completed:nil];
    self.bindingContext = viewModel;
    self.tableView.estimatedRowHeight = 70.0;
    [self.tableView registerNib:[UINib nibWithNibName:@"EPSFormTableViewCell" bundle:nil] forCellReuseIdentifier:CELL_ID];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [(FormsViewModel*)self.bindingContext reload];
    [self.tableView reloadData];
}

#pragma mark - Private Methods

-(NSString*)headerTitleForSecion:(NSInteger)secion
{
    return @"PLEASE SELECT A FORM TO CONTINUE";
}

-(FormModel*)getSelFormModel
{
    NSIndexPath * curSelIndexPath = self.tableView.indexPathForSelectedRow;
    if(nil == curSelIndexPath)
        _form = nil;
    else
    {
        _form = [((FormsViewModel*)self.bindingContext).models objectAtIndex:curSelIndexPath.row];
    }
    
    return _form;
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return ((FormsViewModel*)self.bindingContext).models.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EPSFormTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID forIndexPath:indexPath];
    
    cell.bindingContext = [((FormsViewModel*)self.bindingContext).models objectAtIndex:indexPath.row];

    return cell;
}

#pragma mark - Table View Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"showFormDetailSegue" sender:self];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"showFormDetailSegue"])
    {
        EPSFormTabBarController * dest = (EPSFormTabBarController*)segue.destinationViewController;
        FormViewModel * vm = [[FormViewModel alloc] init];
        [vm initViewModelWithParam:self.form completed:nil];
        dest.bindingContext = vm;
    }
}


@end
