//
//  EPSAttendanceTableViewCell.m
//  EPS
//
//  Created by Yanbo Dang on 31/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSAttendanceTableViewCell.h"
#import "FormAttendanceModel.h"

@interface EPSAttendanceTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segAttendance;
@property (weak, nonatomic) IBOutlet UIButton *btnTime;

- (IBAction)selectTime:(id)sender;
- (IBAction)valueChanged:(id)sender;

-(void)onBindingContextChanged:(FormAttendanceModel*)model;

@end

@implementation EPSAttendanceTableViewCell

@synthesize bindingContext = _bindingContext;
@synthesize dateTime = _dateTime;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setBindingContext:(id)bindingContext
{
    _bindingContext = bindingContext;
    [self onBindingContextChanged:bindingContext];
}

-(void)onBindingContextChanged:(FormAttendanceModel*)model
{
    self.lblName.text = model.EmployeeName;

    self.segAttendance.selectedSegmentIndex = UISegmentedControlNoSegment;
    
    if([model.Attendance isEqualToString:@"Present"])
        self.segAttendance.selectedSegmentIndex = 0;
    if([model.Attendance isEqualToString:@"Absent"])
        self.segAttendance.selectedSegmentIndex = 1;
    if([model.Attendance isEqualToString:@"Late"])
        self.segAttendance.selectedSegmentIndex = 2;
    if([model.Attendance isEqualToString:@"NA"])
        self.segAttendance.selectedSegmentIndex = 3;
    
    [self.btnTime setTitle:model.Comment forState:UIControlStateNormal];
}

-(NSString*)dateTime
{
    return _dateTime = ((FormAttendanceModel*)self.bindingContext).Comment;
}

-(void)setDateTime:(NSString *)dateTime
{
    _dateTime = dateTime;
    [self.btnTime setTitle:dateTime forState:UIControlStateNormal];
    ((FormAttendanceModel*)self.bindingContext).Comment = dateTime;
}

- (IBAction)selectTime:(id)sender {
    
    if([self.delegate respondsToSelector:@selector(requestChangeTime:)])
    {
        [self.delegate requestChangeTime:self];
    }
    
}

- (IBAction)valueChanged:(id)sender {
    
    NSString *answer  = @"";
    
    switch (self.segAttendance.selectedSegmentIndex) {
        case 0:
            answer = @"Present";
            break;
        case 1:
            answer = @"Absent";
            break;
        case 2:
            answer = @"Late";
            break;
        case 3:
            answer = @"NA";
            break;
        default:
            break;
    }
    
    ((FormAttendanceModel*)self.bindingContext).Attendance = answer;
}
@end
