//
//  FormViewModel.h
//  EPS
//
//  Created by Yanbo Dang on 5/9/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import "EPSBaseViewModel.h"

@class FormModel;

@interface FormViewModel : EPSBaseViewModel

@property(nonatomic, strong)FormModel * form;

@end
