//
//  FormCheckListViewModel.m
//  EPS
//
//  Created by Yanbo Dang on 29/8/17.
//  Copyright © 2017 AGCoombs. All rights reserved.
//

#import <AGCFramework/AGCFramework.h>
#import "FormCheckListViewModel.h"
#import "JobViewModel.h"
#import "JobModel.h"
#import "FormModel.h"
#import "FormChecklistModel.h"
#import "MJExtension.h"
#import "PreferenceModel.h"

@interface FormCheckListViewModel()

@property(nonatomic, strong)NSMutableArray * checkList;
@property(nonatomic, weak)FormModel * form;

-(NSArray*)getPreferenceByCategory:(NSString*)category;
-(FormChecklistModel*)getChecklistByJobNum:(NSString*)jobNumber email:(NSString*)email date:(NSString*)sDate prefName:(NSString*)prefName;
-(NSMutableArray*)getChecklistByJobNum:(NSString*)jobNumber email:(NSString*)email date:(NSString*)sDate;

@end

@implementation FormCheckListViewModel

-(instancetype)init
{
    self = [super init];
    return self;
}

-(NSArray*)getPreferenceByCategory:(NSString*)category
{
    __block NSMutableArray * preferences = [[NSMutableArray alloc] init];
    
    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM Preference WHERE Category = '%@' ORDER BY Code ASC",category];
    
    [DBServiceProvider querySql:sSql onDB:self.biz.sDBName queryResult:^(id result, BOOL finished) {
        if(!finished)
        {
            PreferenceModel * pref = [PreferenceModel mj_objectWithKeyValues:result];
            [preferences addObject:pref];
        }
    }];
    
    return preferences;
}

-(FormChecklistModel*)getChecklistByJobNum:(NSString*)jobNumber email:(NSString*)email date:(NSString*)sDate prefName:(NSString*)prefName
{
    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM FormCheckList WHERE JobNumber = '%@' AND SiteForemanEmail = '%@' AND Date = '%@' AND PreferenceName = '%@'", jobNumber, email, sDate, prefName];
    __block FormChecklistModel * model = nil;
    [DBServiceProvider querySql:sSql onDB:self.biz.sDBName queryResult:^(id result, BOOL finished) {
        if(!finished)
            model = [FormChecklistModel mj_objectWithKeyValues:result];
    }];
    return model;
}

-(NSMutableArray*)getChecklistByJobNum:(NSString*)jobNumber email:(NSString*)email date:(NSString*)sDate
{
    NSString * sSql = [NSString stringWithFormat:@"SELECT * FROM FormCheckList WHERE JobNumber = '%@' AND SiteForemanEmail = '%@' AND Date = '%@'", jobNumber, email, sDate];
    
    __block NSMutableArray * list = [[NSMutableArray alloc] init];
    [DBServiceProvider querySql:sSql onDB:self.biz.sDBName queryResult:^(id result, BOOL finished) {
        if(!finished)
           [list addObject:[FormChecklistModel mj_objectWithKeyValues:result]];
    }];
    
    return list;
}

-(void)initViewModelWithParam:(id)param completed:(void (^)(id))completedHandle
{
    JobModel *job  = [JobViewModel getDefaultJob];
    self.form = (FormModel*)param;
    
    NSArray * preferences = [self getPreferenceByCategory:@"QUESTIONNAIRES"];
    
    for (PreferenceModel * pref in preferences)
    {
        FormChecklistModel * model = [self getChecklistByJobNum:job.JobNumber email:job.SiteForemanEmail date:self.form.Date prefName:pref.PreferenceName];
        if (nil == model)
        {
            model = [[FormChecklistModel alloc] initWithJob:job Preference:pref date:self.form.Date];
            [DBServiceProvider insertOrReplaceOnTable:@"FormCheckList" withKeyValuePair:[model mj_keyValues] onDB:self.biz.sDBName];
        }
        
    }
    
    self.checkList = [self getChecklistByJobNum:job.JobNumber email:job.SiteForemanEmail date:self.form.Date];
    
    NSSortDescriptor *sort=[NSSortDescriptor sortDescriptorWithKey:@"SortOrder" ascending:YES];
    [self.checkList  sortUsingDescriptors:[NSArray arrayWithObject:sort]];
}

-(BOOL)save
{
    NSArray * keyvaluepairs = [FormChecklistModel mj_keyValuesArrayWithObjectArray:self.checkList];
    
    return [DBServiceProvider insertOrReplaceOnTable:@"FormCheckList" withKeyValuePairs:keyvaluepairs onDB:self.biz.sDBName];

}

-(id)getCheckListAt:(NSUInteger)index
{
    if(index >= self.checkList.count)
        return nil;
    
    return [self.checkList objectAtIndex:index];
}

-(NSUInteger)count
{
    return self.checkList.count;
}

@end
